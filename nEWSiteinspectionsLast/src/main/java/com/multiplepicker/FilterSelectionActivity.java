package com.multiplepicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.newsiteinspection.R;
import com.newsiteinspection.utility.DialogUtil;
import com.newsiteinspection.utility.FilterAdapter;
import com.newsiteinspection.utility.FilterModel;
import com.newsiteinspection.utility.OnListItemClickCustom;

public class FilterSelectionActivity extends Activity {

	ListView site_name_lv;
	TextView from_date_tv, to_date_tv;
	Button submit_btn, cancel_btn;
	ArrayList<String> sitenameList;
	ArrayList<String> createrList;
	FilterAdapter mFilterAdapter;
	Context mContext;
	FilterModel mFilterModel;
	FilterModel mFilterModelFinal;
	TextView creater_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.filter_selection_activity);
		mContext = FilterSelectionActivity.this;
		initViews();
		getIntentData();
	}

	private void getIntentData() {
		sitenameList = getIntent().getStringArrayListExtra("siteName");
		createrList = getIntent().getStringArrayListExtra("createrList");

		if (null == sitenameList) {
			this.finish();
		}

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		mFilterModel = getIntent().getExtras().getParcelable("fm");
		mFilterModelFinal = mFilterModel;
		mFilterAdapter.SetList(sitenameList);
		if (null != mFilterModelFinal.startDate
				&& !mFilterModelFinal.startDate.equals("")) {
			from_date_tv.setText(mFilterModelFinal.startDate);

		}
		if (null != mFilterModelFinal.endDate
				&& !mFilterModelFinal.endDate.equals("")) {
			to_date_tv.setText(mFilterModelFinal.endDate);

		}

		if (mFilterModelFinal.CreateName.equals("null")) {
			creater_tv.setText("All");
		} else {

			creater_tv.setText(mFilterModelFinal.CreateName);
		}
		if (sitenameList.size() > 0) {
			if (mFilterModelFinal.SiteName.equals("null")) {
				site_name_lv.setItemChecked(0, true);
			} else {
				site_name_lv.setItemChecked(
						sitenameList.indexOf(mFilterModelFinal.SiteName), true);

			}

			mFilterAdapter.notifyDataSetChanged();
		}

	}

	public static Intent getIntent(Context c, ArrayList<String> siteName,
			FilterModel fm, ArrayList<String> createrList) {
		Intent in = new Intent(c, FilterSelectionActivity.class);
		in.putExtra("siteName", siteName);
		in.putExtra("fm", fm);
		in.putExtra("createrList", createrList);
		return in;
	}

	int year, month, day;
	Calendar c;

	private void initViews() {
		c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		sitenameList = new ArrayList<String>();
		createrList = new ArrayList<String>();

		site_name_lv = (ListView) findViewById(R.id.site_name_lv);
		from_date_tv = (TextView) findViewById(R.id.from_date_tv);
		to_date_tv = (TextView) findViewById(R.id.to_date_tv);
		submit_btn = (Button) findViewById(R.id.submit_btn);

		creater_tv = (TextView) findViewById(R.id.creater_tv);
		cancel_btn = (Button) findViewById(R.id.cancel_btn);
		mFilterAdapter = new FilterAdapter(mContext);

		site_name_lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		site_name_lv.setAdapter(mFilterAdapter);

		creater_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				DialogUtil.showListDialog(mContext, 0,
						new OnListItemClickCustom() {

							@Override
							public void onItemClick(int position, String item) {
								// TODO Auto-generated method stub
								if (item.equals("All")) {
									mFilterModel.CreateName="null";
									creater_tv.setText("All");
								} else {
									mFilterModel.CreateName=item;
									creater_tv.setText(item);
								}

							}
						}, createrList);
			}
		});

		site_name_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				String strItem = mFilterAdapter.getItem(position);
				if (strItem.equals("null")) {
					mFilterModel.SiteName = "null";
				} else {
					mFilterModel.SiteName = strItem;
				}

			}

		});

		from_date_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (from_date_tv.getText().toString().equals("From Date")) {

					Toast.makeText(mContext, "Please select start date first",1).show();
				} else {
					showDialog(0);
				}

			}
		});

		to_date_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (from_date_tv.getText().toString().equals("Start Date")) {

					Toast.makeText(mContext, "Please select start date first",
							1).show();
				} else {
					showDialog(1);
				}

			}
		});

		cancel_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent();
				FilterSelectionActivity.this.setResult(RESULT_CANCELED);
				finish();
			}
		});

		submit_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent();
				in.putExtra("fm", mFilterModel);
				FilterSelectionActivity.this.setResult(RESULT_OK, in);
				finish();
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case 0:
			DatePickerDialog dpd = new DatePickerDialog(this, pickerstart,
					year, month, day);
			dpd.setTitle("Start Date");
			return dpd;

		case 1:

			DatePickerDialog dpdq = new DatePickerDialog(this, pickerend, year,
					month, day);
			if (null != mFilterModel.startDate
					&& !mFilterModel.startDate.equals("")) {

				Date dt1;
				try {
					dt1 = df.parse(mFilterModel.startDate);

					dpdq.getDatePicker().setMinDate(dt1.getTime());

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			dpdq.setTitle("End Date");
			return dpdq;

		default:
			break;
		}
		return null;

	}

	SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
	private DatePickerDialog.OnDateSetListener pickerstart = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			c.set(selectedYear, selectedMonth, selectedDay);

			String formattedDate = df.format(c.getTime());
			from_date_tv.setText(formattedDate);

			mFilterModel.startDate = formattedDate;
		}
	};

	private DatePickerDialog.OnDateSetListener pickerend = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			c.set(selectedYear, selectedMonth, selectedDay);
			String formattedDate = df.format(c.getTime());
			to_date_tv.setText(formattedDate);
			mFilterModel.endDate = formattedDate;
		}
	};
}
