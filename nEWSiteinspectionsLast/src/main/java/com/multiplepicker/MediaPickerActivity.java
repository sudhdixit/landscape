package com.multiplepicker;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.newsiteinspection.R;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;



public abstract class MediaPickerActivity extends  Activity {
	public int IMAGE_DIMENSION = 640;

	protected static final int REQ_CODE_PICK_FROM_GALLERY_WITHOUT_CROP = 1;
	protected static final int REQ_CODE_PICK_FROM_CAMERA_WITHOUT_CROP = 2;

	protected static final int REQ_CODE_PICK_FROM_GALLERY_WITH_CROP = 3;
	protected static final int REQ_CODE_PICK_FROM_CAMERA_WITH_CROP = 4;

	protected static final int REQ_CODE_CROP_PHOTO = 5;
	protected static final int REQ_CODE_RECORD_VIDEO = 6;

	private static final String IMAGE_UNSPECIFIED = "image/*";

	private int reqCodeStarter = 0;

	private Uri capturedImageUri, cropImageUri;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	private void initTmpUris() {
		File proejctDirectory = new File(
				Environment.getExternalStorageDirectory() + File.separator
						+ this.getResources().getString(R.string.app_name));
		if (!proejctDirectory.exists()) {
			proejctDirectory.mkdir();
		} /*
		 * else { // delete all old files for (File file :
		 * proejctDirectory.listFiles()) { if
		 * (file.getName().startsWith("tmp_")) { file.delete(); } } }
		 */
		File tempDirectory = new File(proejctDirectory, "temp");
		if (!tempDirectory.exists()) {
			tempDirectory.mkdir();
		} else {
			// delete all old files
			for (File file : tempDirectory.listFiles()) {
				if (file.getName().startsWith("tmp_")
						|| file.getName().startsWith("croped_")) {
					file.delete();
				}
			}

		}
		capturedImageUri = Uri.fromFile(new File(tempDirectory, "tmp_"
				+ String.valueOf(System.currentTimeMillis()) + ".jpg"));

		File extraOutputFile = new File(tempDirectory, "croped_"
				+ String.valueOf(System.currentTimeMillis()) + ".jpg");

		extraOutputFile.setWritable(true);
		cropImageUri = Uri.fromFile(extraOutputFile);
	}

	/**
	 * This method use for take picture from camera and boolean for enable crop
	 * function
	 */
	protected void openCamera(int starterCode, boolean cropAfterSelect) {
		this.reqCodeStarter = starterCode;
		initTmpUris();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				capturedImageUri);
		intent.putExtra("return-data", true);
		try {
			if (cropAfterSelect) {
				startActivityForResult(intent,
						REQ_CODE_PICK_FROM_CAMERA_WITH_CROP);
			} else {
				startActivityForResult(intent,
						REQ_CODE_PICK_FROM_CAMERA_WITHOUT_CROP);
			}

		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
		return;
	}

	/**
	 * This method use for open gallery and crop image using default crop of
	 * android
	 */
	protected void openGallery(int starterCode, boolean cropAfterSelect) {

		this.reqCodeStarter = starterCode;

		if (cropAfterSelect) {
			if (Build.VERSION.SDK_INT < 19) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Complete action using"),
						REQ_CODE_PICK_FROM_GALLERY_WITH_CROP);
			} else {
				Intent intent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setType("image/*");
				startActivityForResult(intent,
						REQ_CODE_PICK_FROM_GALLERY_WITH_CROP);
			}
		} else {
			Intent intent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(Intent.createChooser(intent, "Select File"),
					REQ_CODE_PICK_FROM_GALLERY_WITHOUT_CROP);
		}
	}

	/**
	 * pass time limit in seconds if you unlimited then pass 0
	 */
	protected void openVideoCamera(int durationInSeconds) {
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		if (0 != durationInSeconds) {
			intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, durationInSeconds);
		}
		intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
		startActivityForResult(intent, this.REQ_CODE_RECORD_VIDEO);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode)		{
		case REQ_CODE_PICK_FROM_CAMERA_WITH_CROP:
			if (resultCode == RESULT_OK) {
				cropImage();
			} else {
				onMediaPickCanceled(reqCodeStarter,
						REQ_CODE_PICK_FROM_CAMERA_WITH_CROP);
			}
			break;
		case REQ_CODE_PICK_FROM_GALLERY_WITH_CROP:
			if (resultCode == RESULT_OK && data.getData() != null) {
				initTmpUris();
				capturedImageUri = data.getData();
				cropImage();
			} else {
				onMediaPickCanceled(reqCodeStarter,
						REQ_CODE_PICK_FROM_GALLERY_WITH_CROP);
			}
			break;
		case REQ_CODE_CROP_PHOTO:
			if (resultCode == RESULT_OK) {

				String imagePath = cropImageUri.getPath();
				File file = new File(imagePath);
				onSingleImageSelected(reqCodeStarter, file, imagePath,
						get_Picture_bitmap(file));

			} else {
				onMediaPickCanceled(reqCodeStarter, REQ_CODE_CROP_PHOTO);
			}
			break;
		case REQ_CODE_RECORD_VIDEO:
			if (resultCode == RESULT_OK) {
				Uri vid = data.getData();
				onVideoCaptured(reqCodeStarter, getVideoPathFromURI(vid));
			} else {
				onMediaPickCanceled(reqCodeStarter, REQ_CODE_RECORD_VIDEO);
			}
			break;
		case REQ_CODE_PICK_FROM_GALLERY_WITHOUT_CROP: {
			if (resultCode == RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				File file = new File(picturePath);
				onSingleImageSelected(reqCodeStarter, file, picturePath,
						get_Picture_bitmap(file));
			} else {
				onMediaPickCanceled(reqCodeStarter,
						REQ_CODE_PICK_FROM_GALLERY_WITHOUT_CROP);
			}
		}
			break;

		case REQ_CODE_PICK_FROM_CAMERA_WITHOUT_CROP: {

			if (resultCode == RESULT_OK) {

				String imagePath = capturedImageUri.getPath();
				File file = new File(imagePath);
				onSingleImageSelected(reqCodeStarter, file, imagePath,
						get_Picture_bitmap(file));

			} else {
				onMediaPickCanceled(reqCodeStarter,
						REQ_CODE_PICK_FROM_CAMERA_WITHOUT_CROP);
			}

		}
			break;
		default:
			break;
		}
	}

	public String getRealPathFromURI(Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = this.getContentResolver().query(contentUri, proj, null,
					null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	/*
	 * This method use for Crop image taken from camera
	 */
	private void cropImage() {
		// Use existing crop activity.
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(capturedImageUri, IMAGE_UNSPECIFIED);
		intent.putExtra("outputX", IMAGE_DIMENSION);
		intent.putExtra("outputY", IMAGE_DIMENSION);
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("scale", true);
		intent.putExtra("return-data", true);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, cropImageUri);

		startActivityForResult(intent, REQ_CODE_CROP_PHOTO);
	}

	private String getVideoPathFromURI(Uri contentUri) {
		String videoPath = null;
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(contentUri, proj, null,
				null, null);
		if (cursor.moveToFirst()) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			videoPath = cursor.getString(column_index);
		}
		cursor.close();
		return videoPath;
	}

	public Bitmap get_Picture_bitmap(File nFile) {
		long size_file = getFileSize(nFile);
		size_file = (size_file) / 1000;// in Kb now
		int ample_size = 1;
		if (size_file > 251 && size_file < 1500) {
			ample_size = 2;
		} else if (size_file >= 1500 && size_file < 4500) {
			ample_size = 4;
		} else if (size_file >= 4500 && size_file <= 8000) {
			ample_size = 8;
		} else if (size_file > 8000) {
			ample_size = 16;
		}
		Bitmap bitmap = null;
		BitmapFactory.Options bitoption = new BitmapFactory.Options();
		bitoption.inSampleSize = ample_size;
		Bitmap bitmapPhoto = BitmapFactory.decodeFile(nFile.getAbsolutePath(),
				bitoption);
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(nFile.getAbsolutePath());
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		int orientation = exif
				.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
		Matrix matrix = new Matrix();

		if ((orientation == 3)) {
			matrix.postRotate(180);

		} else if (orientation == 6) {
			matrix.postRotate(90);

		} else if (orientation == 8) {
			matrix.postRotate(270);

		} else {
			matrix.postRotate(0);

		}
		bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0, bitmapPhoto.getWidth(),
				bitmapPhoto.getHeight(), matrix, true);
		return bitmap;
	}

	public long getFileSize(final File file) {
		if (file == null || !file.exists())
			return 0;
		if (!file.isDirectory())
			return file.length();
		final List<File> dirs = new LinkedList<File>();
		dirs.add(file);
		long result = 0;
		while (!dirs.isEmpty()) {
			final File dir = dirs.remove(0);
			if (!dir.exists())
				continue;
			final File[] listFiles = dir.listFiles();
			if (listFiles == null || listFiles.length == 0)
				continue;
			for (final File child : listFiles) {
				result += child.length();
				if (child.isDirectory())
					dirs.add(child);
			}
		}
		return result;
	}

	protected abstract void onSingleImageSelected(int starterCode,
			File fileUri, String imagPath, Bitmap bitmap);

	protected abstract void onVideoCaptured(int starterCode, String videoPath);

	protected abstract void onMediaPickCanceled(int starterCode, int reqCode);

}
