package com.multiplepicker;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class ImageModel implements Parcelable {
	public String id;
	public String user_id;
	public String site;
	public String image_name;
	public String description;
	public String remark;
	public String last_updated_date;
	public String updated_at;
	public String updated_by;
	public String new_inspection_id;
	public String status;
	public String ordersite;
	public String site_name;
	public String remark_image;
	public String checked;

	// {"regionType":"countryID","regionID":"54","regionName":"United States"}
	public ImageModel(JSONObject jsonObject) {
		if (null != jsonObject)
		{
			this.id = jsonObject.optString("id", "");
			this.user_id = jsonObject.optString("user_id", "");
			this.site = jsonObject.optString("site", "");
			this.image_name = jsonObject.optString("image_name", "");
			this.description = jsonObject.optString("description", "");
			this.remark = jsonObject.optString("remark", "");
			this.last_updated_date = jsonObject.optString("last_updated_date",
					"");
			this.updated_at = jsonObject.optString("updated_at", "");
			this.updated_by = jsonObject.optString("updated_by", "");
			this.new_inspection_id = jsonObject.optString("new_inspection_id",
					"");
			this.status = jsonObject.optString("status", "");
			this.ordersite = jsonObject.optString("ordersite", "");
			this.site_name = jsonObject.optString("site_name", "");
			this.remark_image = jsonObject.optString("remark_image", "");
			this.checked=jsonObject.optString("checked","");
		}
	}

	public static ArrayList<ImageModel> getList(JSONArray jsonArray) {
		ArrayList<ImageModel> list = new ArrayList<ImageModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new ImageModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}


	public ImageModel(Parcel in) {
		id = in.readString();
		user_id = in.readString();
		site = in.readString();
		image_name = in.readString();
		id = in.readString();
		user_id = in.readString();
		site = in.readString();
		image_name = in.readString();
		description = in.readString();
		remark = in.readString();
		last_updated_date = in.readString();
		updated_at = in.readString();
		updated_by = in.readString();
		new_inspection_id = in.readString();
		status = in.readString();
		ordersite = in.readString();
		site_name = in.readString();
		remark_image=in.readString();
		checked=in.readString();

	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(user_id);
		dest.writeString(site);

		dest.writeString(image_name);
		dest.writeString(id);
		dest.writeString(user_id);

		dest.writeString(site);
		dest.writeString(image_name);
		dest.writeString(description);

		dest.writeString(remark);
		dest.writeString(last_updated_date);
		dest.writeString(updated_at);

		dest.writeString(updated_by);
		dest.writeString(new_inspection_id);
		dest.writeString(status);

		dest.writeString(ordersite);
		dest.writeString(site_name);
		dest.writeString(remark_image);
		dest.writeString(checked);

	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ImageModel createFromParcel(Parcel in) {
			return new ImageModel(in);
		}

		public ImageModel[] newArray(int size) {
			return new ImageModel[size];
		}
	};

}
