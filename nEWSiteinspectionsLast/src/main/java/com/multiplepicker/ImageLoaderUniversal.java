package com.multiplepicker;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.newsiteinspection.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ImageLoaderUniversal {

	static ImageLoader imageLoader;

	static ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public static DisplayImageOptions normal_Image_diplay_options = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.load_12)
			.showImageOnFail(R.drawable.load_12).cacheOnDisc(true)
			.cacheInMemory(true).resetViewBeforeLoading(true)
			.considerExifParams(true).build();

	public static void ImageLoadSquare(Context mContext, String URl,
			ImageView imgView, DisplayImageOptions Display_options) {

		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
				mContext).defaultDisplayImageOptions(Display_options);

		ImageLoaderConfiguration config = builder.build();

		imageLoader = ImageLoader.getInstance();
		if (!imageLoader.isInited()) {
			imageLoader.init(config.createDefault(mContext));
		}

	 
		imageLoader.displayImage(URl, imgView,Display_options,
				animateFirstListener);

	}

}
