package com.multiplepicker;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class SiteModel1 implements Parcelable {
    public String UserName;
    public String SiteName;
    public String LastUpdateDateTime;
    public String NumberOfPhotos;
    public String ViewReport;
    public String EmailReport;
    public ArrayList<ImageModel1> mImageModelList1;
    public String total_inspection_rerports;


//	UserName: "Rew",
//	SiteName: "Site1",
//	LastUpdateDateTime: "11 March 2016",
//	NumberOfPhotos: "5",
//	ViewReport: "http://chenwa.biz/sadmin/create_report.php?action=view&userid=30&last_updated_date=2016-03-11&site_id=10",
//	EmailReport:


    // {"regionType":"countryID","regionID":"54","regionName":"United States"}
    public SiteModel1(JSONObject jsonObject) {
        if (null != jsonObject) {
            this.UserName = jsonObject.optString("UserName", "");
            this.SiteName = jsonObject.optString("SiteName", "");
            this.LastUpdateDateTime = jsonObject.optString("LastUpdateDateTime", "");
            this.NumberOfPhotos = jsonObject.optString("NumberOfPhotos", "");
            this.ViewReport = jsonObject.optString("ViewReport", "");
            this.EmailReport = jsonObject.optString("EmailReport", "");
            this.total_inspection_rerports = jsonObject.optString("total_inspection_rerports", "");

            try {
                mImageModelList1=ImageModel1.getList(jsonObject.optJSONArray("image_detail"));
            } catch (Exception e) {
                // TODO: handle exception
            }



        }
    }

    public static ArrayList<SiteModel1> getAllSites(JSONArray jsonArray) {
        ArrayList<SiteModel1> list1 = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = 2; i < jsonArray.length(); i++) {
                try {
                    list1.add(new SiteModel1(jsonArray.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list1;
    }

    // "id": "289",
    // "user_id": "30",
    // "site": "10",
    // "image_name": "30_2016219_02122.jpg",
    // "description": "ssss",
    // "remark": "",
    // "last_updated_date": "2016-03-11",
    // "updated_at": "2016-03-11 06:45:07",
    // "updated_by": "30",
    // "new_inspection_id": "8",
    // "status": "1",
    // "ordersite": "0",
    // "site_name": "Site1"

    public SiteModel1(Parcel in) {
        UserName = in.readString();
        SiteName = in.readString();
        LastUpdateDateTime = in.readString();
        NumberOfPhotos = in.readString();
        ViewReport = in.readString();
        EmailReport = in.readString();

        total_inspection_rerports= in.readString();
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserName);
        dest.writeString(SiteName);
        dest.writeString(LastUpdateDateTime);
        dest.writeString(NumberOfPhotos);
        dest.writeString(ViewReport);
        dest.writeString(EmailReport);
        dest.writeString(total_inspection_rerports);


    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public SiteModel1 createFromParcel(Parcel in) {
            return new SiteModel1(in);
        }

        public SiteModel1[] newArray(int size) {
            return new SiteModel1[size];
        }
    };




}
