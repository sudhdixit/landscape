package com.newsiteinspection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.GlobalVariable;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.utility.Adapter;

public class Login extends Activity implements RequestReceiver {
	EditText edtusername = null, edtpassword = null;
	Button submit = null;
	ProgressDialog progDialog;
	int typeBar = 0;
	RelativeLayout lylLodingView = null;
	private Pattern emailPattern;
	private Matcher emailMatcher;
	String checkuser = "", checkpass = "";
	boolean checkstatus = false;
	ImageView imgCheckBox = null;
	TextView txtforgotpassword = null;
	ArrayList<HashMap<String, String>> arrsitelist;
	ArrayList<HashMap<String, String>> sitelist;
	String strusername = "", strpassword = "";
	SharedPreferences prefs = null;
	private LinearLayout checkLay;
	private Context context;
	private int flagService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_screen);
		context=this;
		sitelist = new ArrayList<HashMap<String, String>>();
		arrsitelist = new ArrayList<HashMap<String, String>>();
		initView();
		prefs = Login.this.getSharedPreferences("mobno", 0);
		checkuser = prefs.getString("checkusername", null);
		checkpass = prefs.getString("checkpassword", null);
		System.out.println(checkpass + "  " + checkuser);
		if (checkuser == null && checkpass == null)
		{
			imgCheckBox.setImageResource(R.drawable.checkbox_up);
			checkstatus = false;
		}
		else
		{
			checkstatus = true;
			edtusername.setText(checkuser);
			edtpassword.setText(checkpass);
			imgCheckBox.setImageResource(R.drawable.checkbox_selected);
		}
		setListner();
	
	}

	private void initView() {
		// TODO Auto-generated method stub
		lylLodingView = (RelativeLayout) findViewById(R.id.lylLodingView);
		imgCheckBox = (ImageView) findViewById(R.id.imgCheckBox);
		checkLay = (LinearLayout) findViewById(R.id.checkLay);
		txtforgotpassword = (TextView) findViewById(R.id.txtforgotpassword);
		String htmlString = "<u color=#488947>"	+ txtforgotpassword.getText().toString() + "</u>";
		txtforgotpassword.setText(Html.fromHtml(htmlString));
		edtusername = (EditText) findViewById(R.id.edtusername);
		edtpassword = (EditText) findViewById(R.id.edtpassword);
		submit = (Button) findViewById(R.id.submit);
	}

	private void initData() {
		// TODO Auto-generated method stub

		strusername = edtusername.getText().toString();
		strpassword = edtpassword.getText().toString();
		if (strusername.equals("")) {
			Constants.ShowToast(Login.this,"Please enter email address");
		} else if (eMailValidation(edtusername.getText().toString()) == false) {
			edtusername.setText("");
			Constants.ShowToast(Login.this, "Please enter valid email address");
		} else if (strpassword.equals(""))
		{
			Constants.ShowToast(Login.this, "Please enter password");
		} else {
			Editor editor1 = null;
			if (checkstatus == true) {
				editor1 = prefs.edit();
				editor1.putString("checkusername", strusername);
				editor1.putString("checkpassword", strpassword);
				editor1.commit();

			} else {
				editor1 = prefs.edit();
				editor1.putString("checkusername", null);
				editor1.putString("checkpassword", null);
				editor1.commit();
			}
			NetReachability net = new NetReachability(Login.this);
			boolean netcheck = net.isInternetOn();
			if (netcheck == false) {
				netCheckConfirmation_dialog();
			} else {
				lylLodingView.setVisibility(View.VISIBLE);
				flagService = 1;
				WebserviceHelper helper = new WebserviceHelper(Login.this,Constants.GLOBAL_URL_NEW);
				helper.addParam("action", "login_new");
				helper.addParam("username", strusername);
				helper.addParam("password", strpassword);
				helper.execute(helper.buildUrl());
			}
		}
	}

	public boolean eMailValidation(String email) {
		emailPattern = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}"
				+ "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
				+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");
		emailMatcher = emailPattern.matcher(email);
		return emailMatcher.matches();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// Home.this.overridePendingTransition(R.anim.slide_back_in,
		// R.anim.slide_back_out);
		Login.this.finish();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			progDialog = new ProgressDialog(this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Please wait...");
			progDialog.setCanceledOnTouchOutside(false);
			progDialog.setCancelable(true);
			return progDialog;
		default:
			return null;
		}
	}

	public void networkPopup() {
		// this intent class using network checked
		Intent i = new Intent(Login.this, NetworkPopup.class);
		startActivity(i);
	}

	private void setListner() {
		// TODO Auto-generated method stub
		checkLay.setOnClickListener(laylistner);
		submit.setOnClickListener(laylistner);
		txtforgotpassword.setOnClickListener(laylistner);
	}

	OnClickListener laylistner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.submit: {
				try {
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getCurrentFocus()
							.getWindowToken(), 0);
				} catch (Exception e) {

				}
				initData();
			}
				break;
			case R.id.txtforgotpassword: {
				final Dialog dialog = new Dialog(Login.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCanceledOnTouchOutside(false);
				dialog.setContentView(R.layout.forgotpassword);

				// set the custom dialog components - text, image and button
				final EditText edtforgotemail = (EditText) dialog
						.findViewById(R.id.edtforgotemail);
				final Button btnForgotPasswordCancel = (Button) dialog
						.findViewById(R.id.btnForgotPasswordCancel);
				final Button btnForgotPasswordDone = (Button) dialog
						.findViewById(R.id.btnForgotPasswordDone);
				btnForgotPasswordCancel
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								// Auto-generated method stub
								dialog.dismiss();
							}
						});
				dialog.setOnShowListener(new OnShowListener() {

					@Override
					public void onShow(DialogInterface arg0) {
						// TODO Auto-generated method stub
						edtforgotemail.setCursorVisible(true);
						InputMethodManager imm = (InputMethodManager) getSystemService(Login.this.INPUT_METHOD_SERVICE);
						imm.showSoftInput(edtforgotemail,
								InputMethodManager.SHOW_IMPLICIT);
						edtforgotemail.requestFocus();
					}
				});
				btnForgotPasswordDone.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Auto-generated method status
						if (edtforgotemail.getText().toString().length() == 0) {
							Constants.ShowToast(Login.this, "Please enter email");
						} else if (eMailValidation(edtforgotemail.getText()
								.toString()) == false) {
							Constants.ShowToast(Login.this, "Please enter valid email address");
						} else {
							dialog.dismiss();
							NetReachability net = new NetReachability(
									Login.this);
							boolean netcheck = net.isInternetOn();
							if (netcheck == false) {
								networkPopup();
							} else {
								flagService = 3;
								// this service called forget password
								dialog.dismiss();
								showDialog(typeBar);
								WebserviceHelper helper = new WebserviceHelper(Login.this,Constants.GLOBAL_URL);
								helper.addParam("action", "forgetPassword");
								helper.addParam("username", edtforgotemail
										.getText().toString());
								helper.execute(helper.buildUrl());
							}
						}
						// .http://apmocon.com/webservice/webservices.php?action=forgetPassword&email=neeraj.rathore596@gmail.com
					}
				});
				dialog.show();
			}
				break;
			case R.id.checkLay: {
				if (checkstatus == false) {
					checkstatus = true;

					imgCheckBox.setImageResource(R.drawable.checkbox_selected);
				} else {
					checkstatus = false;
					imgCheckBox.setImageResource(R.drawable.checkbox_up);

				}
			}
				break;
			}
		}

	};

	private void netCheckConfirmation_dialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Login.this);

		alertDialog
				.setMessage("No Internet Connection Available. Do you want to try again?");
		alertDialog.setCancelable(false);
		alertDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						NetReachability net = new NetReachability(Login.this);
						boolean netcheck = net.isInternetOn();
						if (netcheck == false) {
							netCheckConfirmation_dialog();
						} else {
							dialog.dismiss();
						}
					}
				});

		alertDialog.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to execute after dialog
						dialog.dismiss();
						Login.this.finish();
					}
				});
		alertDialog.show();

	}

	private void getsite() {
		// TODO Auto-generated method stub
		flagService = 2;
		WebserviceHelper helper = new WebserviceHelper(Login.this, Constants.GLOBAL_URL);
		helper.addParam("action", "sitesList");
		helper.addParam("user_id",AppUtil.getUserId(context));
		helper.execute(helper.buildUrl());

	}

	@Override
	public void requestFinished(String result) {
		// TODO Auto-generated method stub
		try {
			if (flagService == 1)
			{
				if(result!=null)
				{
					JSONObject res = new JSONObject(result);
					String status = res.optString("status", "");
					if (!TextUtils.isEmpty(status) && status.equals("true"))
					{
						lylLodingView.setVisibility(View.GONE);
						JSONObject json_data = res.optJSONObject("result");
						AppUtil.setUserEmail(context, edtusername.getText().toString());
						AppUtil.setUserName(context, json_data.optString("username", ""));
						AppUtil.setPassword(context, strpassword);
						AppUtil.setLoginStatus(context, "IN");
						AppUtil.setUserId(context, json_data.getString("user_id"));
						AppUtil.setUserType(context, json_data.optString("user_type", ""));
						getsite();
					}
					else
					{
						lylLodingView.setVisibility(View.GONE);
						Intent i = new Intent(this, AlertPopupScreen.class);
						i.putExtra("utilityTitle", "Login Error");
						i.putExtra("utilityMessage", "Username or Password Does not Match");
						startActivity(i);
					}
				}
				else
				{
					lylLodingView.setVisibility(View.GONE);
					Intent i = new Intent(this, AlertPopupScreen.class);
					i.putExtra("utilityTitle", "Login Error");
					i.putExtra("utilityMessage", "Username or Password Does not Match");
					startActivity(i);
				}

			} else if (flagService == 2)
			{
				JSONObject jsonObject = new JSONObject(result);
				JSONArray res = jsonObject.getJSONArray("W");
				String status = res.getJSONObject(0).getString("Status");
				if (status.equals("false"))
				{ // result failed error show in
					// pop_up
					lylLodingView.setVisibility(View.GONE);
					Intent i = new Intent(this, AlertPopupScreen.class);
					i.putExtra("utilityTitle", "Login Error");
					i.putExtra("utilityMessage",
							"Error while connecting server");
					startActivity(i);
				}
				else if (status.equals("true"))
				{
					lylLodingView.setVisibility(View.GONE);
					Adapter mdbhelper = new Adapter(Login.this);
					mdbhelper.open();
					mdbhelper.deletesite();
					for (int i1 = 1; i1 < res.length(); i1++)
					{
						JSONObject json_data1 = res.getJSONObject(i1);
						ContentValues cv = new ContentValues();
						HashMap<String, String> map = new HashMap<String, String>();
						cv.put("site_id", json_data1.getString("id"));
						cv.put("site_name",json_data1.getString("site_name"));
						cv.put("site_date",json_data1.getString("inspection_date"));
						cv.put("site_count",json_data1.getString("sitecounts"));
						mdbhelper.addsite(cv);
					}
					mdbhelper.close();
					Intent i = new Intent(Login.this,Home.class);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);
					finish();
				}

			}
			else if (flagService == 3)
			{
				lylLodingView.setVisibility(View.GONE);
				JSONObject jsonObject = new JSONObject(result);
				JSONArray res = jsonObject.getJSONArray("W");
				String status = res.getJSONObject(0).getString("Status");
				if (status.equals("false"))
				{
					Intent i = new Intent(this, AlertPopupScreen.class);
					i.putExtra("utilityTitle", "Error");
					i.putExtra("utilityMessage", "Username is not registered");
					startActivity(i);
				} else if (status.equals("true"))
				{
					System.out.println("heeeeeeeeeeeeeeeeee");
					progDialog.dismiss();
					Constants.ShowToast(Login.this, "Please check your email address");
				}
			}
		}
		catch (JSONException e) {
			lylLodingView.setVisibility(View.GONE);
		}
		catch (Exception e) {
			lylLodingView.setVisibility(View.GONE);
			networkPopup();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		GlobalVariable globleLatLong = ((GlobalVariable) getApplicationContext());
		globleLatLong.clears();
	}
}
