package com.newsiteinspection.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.quotemodule.QuotationImageList;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.apputility.GlobalAlertDialogs;
import com.newsiteinspection.apputility.HttpCall;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.model.QuoteModel;
import com.newsiteinspection.model.QuoteMonthModel;
import com.newsiteinspection.quotemodule.QuotesListingActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sudh on 9/23/2016.
 */
public class QuoteListAdapter extends BaseExpandableListAdapter
{
    private Context context;
    private ArrayList<QuoteMonthModel> adapterList;
    private QuotesListingActivity mActivity;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

    public QuoteListAdapter(Context context,QuotesListingActivity activity,ArrayList<QuoteMonthModel> list) {
        this.context = context;
        this.adapterList = list;
        this.mActivity=activity;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<QuoteModel> productList = adapterList.get(groupPosition).getQuoteList();
        return productList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition,final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        final QuoteModel detailInfo = (QuoteModel) getChild(groupPosition, childPosition);
        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.particular_site_item, null);
        }
        ImageView listediticon = (ImageView) convertView.findViewById(R.id.listediticon);
        ImageView particular_site_image = (ImageView) convertView.findViewById(R.id.particular_site_image);
        ImageView listdelete = (ImageView)  convertView.findViewById(R.id.listdelete);
        ImageView mail_iv = (ImageView) convertView.findViewById(R.id.mail_iv);
        ImageView view_report_iv = (ImageView)  convertView.findViewById(R.id.view_report_iv);
        TextView imagecount = (TextView)  convertView.findViewById(R.id.imagecount);
        TextView user_name = (TextView)  convertView.findViewById(R.id.user_name);
        TextView sitename = (TextView)  convertView.findViewById(R.id.sitename);
        TextView date = (TextView)  convertView.findViewById(R.id.date);

        sitename.setText(detailInfo.getSiteName());
        if(!TextUtils.isEmpty(detailInfo.getLastUpdateDateTime()))
            date.setText(detailInfo.getLastUpdateDateTime());
        imagecount.setText(detailInfo.getNumberOfPhotos());
        user_name.setText(detailInfo.getUserName());
        view_report_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try {
                    if (!TextUtils.isEmpty(detailInfo.getViewReport())) {

                        Intent myIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(detailInfo.getViewReport()));
                        context.startActivity(myIntent);
                    }

                } catch (ActivityNotFoundException e) {
                    Constants.ShowToast(context, "No application can handle this request."
                            + " Please install a webbrowser");
                    e.printStackTrace();
                }

            }
        });

        mail_iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                String pass = "Please download your quote report\n"
                        + " " + detailInfo.getEmailReport();
                intent.putExtra(Intent.EXTRA_TEXT, pass);
                intent.setType("text/html");
                context.startActivity(Intent.createChooser(intent, "Send mail"));
            }
        });

        listdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(R.layout.confirmation);
                TextView txtTitle = (TextView) dialog
                        .findViewById(R.id.txtTitle);
                TextView txtdescription = (TextView) dialog
                        .findViewById(R.id.txtdescription);
                txtTitle.setText("Delete Quote");
                txtdescription
                        .setText("Are you sure you want to delete this quote and all sites in this quote?");
                // String array for issue title
                // set the custom dialog components - List-view
                // btn-cancel
                final Button btncancel = (Button) dialog
                        .findViewById(R.id.btncancel);
                final Button btnDone = (Button) dialog
                        .findViewById(R.id.btnDone);
                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        // Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        dialog.dismiss();
                        String url=Constants.GLOBAL_URL+"action=deletequotationOfSite&site_id="+detailInfo.getSite_id()+"&last_update_date="+detailInfo.getImageList().get(0).last_updated_date+"&user_id="+detailInfo.getUser_id();
                        if (Constants.isNetworkAvailable(context))
                        {
                            String[] params = new String[3];
                            params[0] = url;
                            params[1] = "" + groupPosition;
                            params[2] = "" + childPosition;
                            new DeleteQuotationList().execute(params);
                        }
                        else
                            Constants.ShowNetworkError(context);
                    }
                });
                dialog.show();
            }
        });
        listediticon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                NetReachability net = new NetReachability(
                        context);
                boolean netcheck = net.isInternetOn();
                if (netcheck == false) {
                    Toast.makeText(
                            context,
                            "Please  check your internet connection and try again",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (detailInfo.getNumberOfPhotos().equals("0"))
                    {
                        Toast.makeText(context,"No Quote", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Intent i=new Intent(context,QuotationImageList.class);
                        i.putExtra("site_id",detailInfo.getSite_id());
                        i.putExtra("last_updated_date",detailInfo.getImageList().get(0).last_updated_date);
                        i.putExtra("user_id",detailInfo.getUser_id());
                        i.putExtra("filter_status","1");
                        mActivity.startActivityForResult(i,12345);
                    }
                }
            }
        });


        particular_site_image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                NetReachability net = new NetReachability(
                        context);
                boolean netcheck = net.isInternetOn();
                if (netcheck == false) {
                    Toast.makeText(
                            context,
                            "Please  check your internet connection and try again",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (detailInfo.getNumberOfPhotos().equals("0"))
                    {
                        Toast.makeText(context,"No Quote ", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Intent i=new Intent(context,QuotationImageList.class);
                        i.putExtra("site_id",detailInfo.getSite_id());
                        i.putExtra("last_updated_date",detailInfo.getImageList().get(0).last_updated_date);
                        i.putExtra("user_id",detailInfo.getUser_id());
                        i.putExtra("filter_status","1");
                        mActivity.startActivityForResult(i,12345);
                    }
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        ArrayList<QuoteModel> productList = adapterList.get(groupPosition).getQuoteList();
        return productList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return adapterList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return adapterList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        QuoteMonthModel headerInfo = (QuoteMonthModel)getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.list_group, null);
        }

        TextView sequence = (TextView) view.findViewById(R.id.lblListHeader);
        sequence.setText(headerInfo.getMonthName());
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class DeleteQuotationList extends AsyncTask<String, Void, String>
    {
        boolean error=false;
        String errorMessage="";
        int groupPosition;
        int childPosition;
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            groupPosition=Integer.valueOf(params[1]);
            childPosition=Integer.valueOf(params[2]);
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.callJsnGet(params[0]);
            if (resp != null) {
                try {
                    JSONObject jo = new JSONObject(resp);
                    JSONArray joW = jo.optJSONArray("W");
                    if(joW.optJSONObject(0)!=null && joW.optJSONObject(0).optString("Status","").equalsIgnoreCase("true"))
                    {
                        return "completed";
                    }
                    else {
                        error = true;
                        errorMessage = "No Quote found";
                    }
                } catch (Exception e)
                {
                    error=true;
                    errorMessage=Constants.SERVER_EXCEPTION_MSG;
                }
            }
            else
            {
                error=true;
                errorMessage=Constants.TIMEOUT_ERROR;
            }
            return "completed";
        }

        protected void onPostExecute(String resp)
        {
            Constants.hideProgressDialog();
            if (!TextUtils.isEmpty(resp))
            {
                if (!error)
                {
                    GlobalAlertDialogs.createAlertSingle((Activity) context, "Quote Deleted Successfully", "OK", false);
                    adapterList.get(groupPosition).getQuoteList().remove(childPosition);
                    notifyDataSetChanged();
                }
                else
                    GlobalAlertDialogs.createAlertSingle((Activity) context, errorMessage, "OK", false);
            }
            else
                GlobalAlertDialogs.createAlertSingle((Activity) context,"There is some error in request", "OK", false);
        }
    }
}
