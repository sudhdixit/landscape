package com.newsiteinspection.quotemodule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.newsiteinspection.inspectionmodule.AddMultipleInspectionActivity;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.utility.Adapter;

public class AddNewQuotation1 extends Activity {
    private Context context;
    TextView username = null, txtchoosesite = null, Photo = null, takephoto = null;
    private EditText et;
    String site_id = "";
    Button nextfirst = null;
    Button btnback = null, home = null;
    ImageView siteimage = null;
    String site_name = "";
    ArrayList<String> sitename = null;
    ArrayList<String> siteids = null;
    private Dialog dialog;
    private ListView sitelist;
    private ArrayAdapter<String> issueAdapter;
    public final static int SELECT_FILE = 310, SELECT_CAMERA = 311;
    private String selectedPath;
    ProgressDialog progDialog;
    private String created_by,last_updated_date,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.addnewquotation1);
        context = this;
        et = (EditText) findViewById(R.id.title_txt);
        int textLength = et.getText().length();
        et.setSelection(textLength, textLength);
        site_id=getIntent().getStringExtra("site_id");
        created_by=getIntent().getStringExtra("created_by");
        last_updated_date=getIntent().getStringExtra("last_updated_date");
        title=getIntent().getStringExtra("title");
        if(!TextUtils.isEmpty(title))
            et.setText(title.toUpperCase());
        if(!TextUtils.isEmpty(site_id))
        {
            Adapter mdbhelper=new Adapter(context);
            mdbhelper.open();
            Cursor cr = mdbhelper.getSiteName(site_id);
            if(cr.getCount()>0)
            {
                cr.moveToFirst();
                site_name = cr.getString(0);
            }
            mdbhelper.close();
        }
        initView();
        initData();
        setListner();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                progDialog = new ProgressDialog(this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.setCancelable(true);
                return progDialog;
            default:
                return null;
        }
    }

    private void initView() {
        // TODO Auto-generated method stub
        siteimage = (ImageView) findViewById(R.id.siteimage);
        username = (TextView) findViewById(R.id.username);
        username.setText("Welcome " + AppUtil.getUserName(context) + " !");
        txtchoosesite = (TextView) findViewById(R.id.txtchoosesite);
        Photo = (TextView) findViewById(R.id.Photo);
        takephoto = (TextView) findViewById(R.id.takephoto);
        nextfirst = (Button) findViewById(R.id.nextfirst);
        btnback = (Button) findViewById(R.id.btnback);
        home = (Button) findViewById(R.id.home);
    }

    private void initData() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(site_name)) {
            txtchoosesite.setText("Select Site");
        } else {
            txtchoosesite.setText(site_name);
        }
    }

    private void setListner() {
        // TODO Auto-generated method stub
        txtchoosesite.setOnClickListener(laylistner);
        Photo.setOnClickListener(laylistner);
        takephoto.setOnClickListener(laylistner);
        nextfirst.setOnClickListener(laylistner);
        btnback.setOnClickListener(laylistner);
        home.setOnClickListener(laylistner);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
    }

    OnClickListener laylistner = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.txtchoosesite: {
                    dialogsiteList();
                }
                break;
                case R.id.btnback: {
                    onBackPressed();
                }
                case R.id.home: {
                    Intent i=new Intent(context,Home.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;
                case R.id.Photo: {
                    if (TextUtils.isEmpty(site_id))
                    {
                        Toast.makeText(AddNewQuotation1.this, "Please select site", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        single_multiple();
                    }
                }
                break;
                case R.id.takephoto: {
                    doTakePhotoAction();
                }
                break;
                case R.id.nextfirst: {
                    if (nextfirst.getText().toString().equalsIgnoreCase("Next"))
                    {
                        if (TextUtils.isEmpty(site_id))
                        {
                            Toast.makeText(AddNewQuotation1.this, "Please select site", Toast.LENGTH_LONG).show();
                        } else if (TextUtils.isEmpty(selectedPath)) {
                            Toast.makeText(AddNewQuotation1.this, "Please select image", Toast.LENGTH_LONG).show();
                        } else
                        {
                            String titletxt = null;
                            if (et.getText().toString() != null)
                            {
                                titletxt = et.getText().toString().trim();
                            }
                            Intent i = new Intent(AddNewQuotation1.this, AddNewQuotation2.class);
                            i.putExtra("site_id",site_id);
                            i.putExtra("site_name", site_name);
                            i.putExtra("imagepath",selectedPath);
                            i.putExtra("title", titletxt);
                            i.putExtra("last_updated_date",last_updated_date);
                            i.putExtra("created_by",created_by);
                            startActivityForResult(i,Constants.REQUEST_ADD_QUOTE_2);
                        }
                    }
                }
                break;
            }
        }
    };


    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }


    private void doTakePhotoAction() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(cameraIntent, SELECT_CAMERA);
    }

    public void single_multiple() {
        dialog = new Dialog(AddNewQuotation1.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_multiple_selection);
        final Button btn_single = (Button) dialog.findViewById(R.id.btn_single);
        final Button btn_multiple = (Button) dialog.findViewById(R.id.btn_multiple);

        btn_single.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Auto-generated method stub
                nextfirst.setText("Next");
                dialog.dismiss();
                openGallery();
            }
        });
        btn_multiple.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                NetReachability net = new NetReachability(AddNewQuotation1.this);
                if (net.isInternetOn()) {
                    dialog.dismiss();
                    String titletxt = null;
                    if (et.getText().toString() != null)
                    {
                        titletxt = et.getText().toString().trim();
                    }
                    Intent i = new Intent(AddNewQuotation1.this, AddMultipleQuotationActivity.class);
                    i.putExtra("site_id",site_id);
                    i.putExtra("site_name", site_name);
                    i.putExtra("title", titletxt);
                    i.putExtra("last_updated_date",last_updated_date);
                    i.putExtra("created_by", created_by);
                    startActivityForResult(i,Constants.REQUEST_ADD_MULTIPLE_IMAGE_QUOTE);
                }
                else
                {
                    Toast.makeText(AddNewQuotation1.this,"You cant select multiple images when internet connection is off",Toast.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();

    }

    public void dialogsiteList() {
        sitename = new ArrayList<String>();
        siteids = new ArrayList<String>();
        Adapter mdbhelper = new Adapter(AddNewQuotation1.this);
        mdbhelper.open();
        Cursor c = null;
        c = mdbhelper.getAllSite();
        if (c.moveToFirst()) {
            do {
                siteids.add(c.getString(1));
                sitename.add(c.getString(2));
            } while (c.moveToNext());

        }
        c.close();
        mdbhelper.close();

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.sitelist);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation1;
        // set the custom dialog components - text, image and button

        ImageButton close = (ImageButton) dialog.findViewById(R.id.close);
        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        Setsitedata();
        dialog.show();

    }

    public void Setsitedata() {

        issueAdapter = new ArrayAdapter<String>(this, R.layout.siteitem,
                sitename);
        sitelist = (ListView) dialog.findViewById(R.id.lstSiteList);
        sitelist.setAdapter(issueAdapter);
        sitelist.setChoiceMode(1);
        sitelist.setItemChecked(sitename.indexOf(site_name), true);
        sitelist.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {

                dialog.dismiss();
                site_name = sitelist.getItemAtPosition(pos).toString();
                txtchoosesite.setText(site_name);
                site_id = siteids.get(pos);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ADD_QUOTE_2)
            {
                Intent i = new Intent();
                i.putExtra("site_id",data.getStringExtra("site_id"));
                i.putExtra("last_updated_date",data.getStringExtra("last_updated_date"));
                i.putExtra("user_id",data.getStringExtra("user_id"));
                setResult(RESULT_OK,i);
                finish();
            }
            else if (requestCode == Constants.REQUEST_ADD_MULTIPLE_IMAGE_QUOTE)
            {
                Intent i = new Intent();
                i.putExtra("site_id",data.getStringExtra("site_id"));
                i.putExtra("last_updated_date",data.getStringExtra("last_updated_date"));
                i.putExtra("user_id",data.getStringExtra("user_id"));
                setResult(RESULT_OK,i);
                finish();
            }
            if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                selectedPath = getImagePath(selectedImageUri);
                if (!TextUtils.isEmpty(selectedPath)) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    selectedPath = Constants.compressImage(selectedPath, context);
                    siteimage.setImageBitmap(BitmapFactory.decodeFile(selectedPath, options));
                } else
                    Constants.ShowToast(context, "No File Selected");
            }
            else if (requestCode == SELECT_CAMERA)
            {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                selectedPath = f.getAbsolutePath();
                if (!TextUtils.isEmpty(selectedPath)) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    selectedPath = Constants.compressImage(selectedPath, context);
                    siteimage.setImageBitmap(BitmapFactory.decodeFile(selectedPath, options));
                } else
                    Constants.ShowToast(context, "No File Selected");
            }
        }
    }

    public String getImagePath(Uri uri)
    {
        String result = "";
        boolean isok = false;
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(uri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            isok = true;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isok ? result : "";
    }
}
