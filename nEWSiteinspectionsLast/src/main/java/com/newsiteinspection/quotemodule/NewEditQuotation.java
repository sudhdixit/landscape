package com.newsiteinspection.quotemodule;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.multiplepicker.ImageModel1;
import com.multiplepicker.MediaPickerActivity;
import com.newsiteinspection.FullViewImage;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.ImageLoader;
import com.newsiteinspection.helper.ImageUpload;
import com.newsiteinspection.helper.Receiver;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;

@SuppressWarnings("unused")
public class NewEditQuotation extends MediaPickerActivity implements
        OnClickListener, Receiver, RequestReceiver {

    private EditText edtdescription = null, edtremarks = null;
    private Button capture = null, browse = null, save = null, cancel,
            home = null;
    private Button btnback;
    private ImageView siteimage = null;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.editquotation);
        mContext = NewEditQuotation.this;
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getIntentData();
        initViews();
        setData();

    }

    String description, remarks, imgname;
    private ImageModel1 mImageModel;
    String localPath = "";

    public void setData() {

        if (null != mImageModel) {
            edtdescription.setText(mImageModel.description);
            edtremarks.setText(mImageModel.remark);
            imageName = mImageModel.image_name;

            mImageLoader.DisplayImage(
                    "http://chenwa.biz/webservices/imagesuploaded/"
                            + mImageModel.image_name, siteimage);
        }
    }

    private void getIntentData() {
        mImageModel = (ImageModel1) getIntent().getParcelableExtra("imgModel");

    }

    public static Intent getIntent(Context context, ImageModel1 imgModel) {

        Intent intent = new Intent(context, NewEditQuotation.class);
        intent.putExtra("imgModel", imgModel);
        return intent;

    }

    private ImageLoader mImageLoader;

    private void initViews() {
        edtdescription = (EditText) findViewById(R.id.edtdescription);
        edtremarks = (EditText) findViewById(R.id.edtremarks);
        capture = (Button) findViewById(R.id.capture);
        browse = (Button) findViewById(R.id.browse);
        btnback = (Button) findViewById(R.id.btnback);
        home = (Button) findViewById(R.id.home);
        save = (Button) findViewById(R.id.save);
        cancel = (Button) findViewById(R.id.cancel);
        siteimage = (ImageView) findViewById(R.id.siteimage);
        mImageLoader = new ImageLoader(this);
        capture.setOnClickListener(this);
        siteimage.setOnClickListener(this);
        browse.setOnClickListener(this);
        btnback.setOnClickListener(this);
        home.setOnClickListener(this);
        cancel.setOnClickListener(this);
        save.setOnClickListener(this);

    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.capture:
                openGallery(222, false);
                break;
            case R.id.browse:
                openGallery(111, false);
                break;
            case R.id.btnback:
                this.finish();
                break;
            case R.id.home:
                Intent i=new Intent(NewEditQuotation.this,Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            case R.id.save: {
                CallWebServiceSave();

            }

            break;
            case R.id.cancel: {
                this.finish();
            }

            break;
            case R.id.siteimage: {

                Intent i1 = new Intent(NewEditQuotation.this, FullViewImage.class);
                i1.putExtra("FRM", "SERVER");
                if (localPath.equals("")) {
                    i1.putExtra("URL",
                            "http://chenwa.biz/webservices/imagesuploaded/"
                                    + mImageModel.image_name);
                } else {
                    i1.putExtra("URL", "file://" + localPath);
                }
                startActivity(i1);
            }

            break;

            default:
                break;
        }

    }

    private void CallWebServiceSave() {

        showDialog(0);
        if (localPath.length() > 0) {
            ImageUpload upload1 = new ImageUpload(this);
            upload1.execute(localPath, imageName, "image");
        }
        WebserviceHelper helper = new WebserviceHelper(this, Constants.GLOBAL_URL);
        SharedPreferences prefs = this.getSharedPreferences("mobno", 0);
        String ifd = prefs.getString("uid", "0");
        helper.addParam("action", "tempeditquotation");
        helper.addParam("inspection_id", mImageModel.id);
        helper.addParam("image_name", imageName);
        helper.addParam("user_id", ifd);
        helper.addParam("description", edtdescription.getText().toString().trim());
        helper.addParam("remark", edtremarks.getText().toString().trim());
        helper.addParam("title", mImageModel.title);
        helper.execute(helper.buildUrl());
    }

    String imageName = "";

    @Override
    protected void onSingleImageSelected(int starterCode, File fileUri,
                                         String imagPath, Bitmap bitmap) {

        if (null != bitmap) {
            siteimage.setImageBitmap(bitmap);
            localPath = imagPath;
            imageName = fileUri.getName();
        }

    }

    @Override
    protected void onVideoCaptured(int starterCode, String videoPath) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onMediaPickCanceled(int starterCode, int reqCode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void requestFinish(String result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void requestFinished(String result) {

        // TODO Auto-generated method stub
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray res = jsonObject.getJSONArray("W");
            String status = res.getJSONObject(0).getString("Status");
            mProgressDialog.dismiss();
            if (status.equals("false")) { // result failed error show

                Intent i = new Intent(this, AlertPopupScreen.class);
                i.putExtra("utilityTitle", "Unable To Add site ...!");
                i.putExtra("utilityMessage", "Please add site after sometime");
                startActivity(i);
            } else if (status.equals("true")) {
                Toast.makeText(NewEditQuotation.this, "Edit Successfull", Toast.LENGTH_LONG)
                        .show();
                setResult(RESULT_OK);
                this.finish();

            }

        } catch (JSONException e) {
            // TODO: handle exception
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    ProgressDialog mProgressDialog;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Please wait...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setCancelable(false);
                return mProgressDialog;
            default:
                return null;
        }
    }
}
