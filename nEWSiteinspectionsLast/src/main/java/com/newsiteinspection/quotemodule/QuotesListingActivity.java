package com.newsiteinspection.quotemodule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.multiplepicker.FilterSelectionActivity;
import com.newsiteinspection.R;
import com.newsiteinspection.adapter.QuoteListAdapter;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.apputility.HttpCall;
import com.newsiteinspection.json.JsonParser;
import com.newsiteinspection.model.QuoteMonthModel;
import com.newsiteinspection.utility.Adapter;
import com.newsiteinspection.utility.FilterModel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ayush on 5/8/16.
 */
public class QuotesListingActivity extends Activity{
    private Context context;
    ExpandableListView expandibleListView;
    private ArrayList<QuoteMonthModel> quoteMonthList;
    String userId;
    Button btnback = null, home = null;
    ArrayList<String> iniqueSIteArr = new ArrayList<String>();
    ArrayList<String> createrList = new ArrayList<String>();
    Adapter mdbhelper;
    private RelativeLayout nodata;
    private QuoteListAdapter adapter;
    private TextView site_inspection_tv;
    private FilterModel mFilterModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_quote_listing);
        context=this;
        mdbhelper = new Adapter(context);
        getAllSites();
        getAllCreaters();
        userId= AppUtil.getUserId(context);
        mFilterModel = new FilterModel();
        home = (Button) findViewById(R.id.home);
        home.setOnClickListener(mClickListener);
        btnback = (Button) findViewById(R.id.btnback);
        btnback.setOnClickListener(mClickListener);
        nodata=(RelativeLayout)findViewById(R.id.nodata);
        site_inspection_tv=(TextView)findViewById(R.id.site_inspection_tv);
        expandibleListView=(ExpandableListView)findViewById(R.id.listview);
        expandibleListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
        {
            @Override
            public boolean onGroupClick(ExpandableListView parent,View v,int groupPosition,long id) {
                return false;
            }
        });
        // Listview Group expanded listener
        expandibleListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition)
            {
            }
        });

        // Listview Group collasped listener
        expandibleListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        // Listview on child click listener
        expandibleListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
                return false;
            }
        });

        quoteMonthList=new ArrayList<>();
        adapter=new QuoteListAdapter(context,QuotesListingActivity.this,quoteMonthList);
        expandibleListView.setAdapter(adapter);
        if(Constants.isNetworkAvailable(context))
        {
            String url = Constants.GLOBAL_URL_NEW+"action=tempqutation";
            new GetQuotationList().execute(url);
        }
        else
            Constants.ShowNetworkError(context);

    }



    public void SORTBY(View v) {

        startActivityForResult(FilterSelectionActivity.getIntent(
                QuotesListingActivity.this,iniqueSIteArr, mFilterModel,
                createrList), 5555);

    }

    public void getAllSites() {
        iniqueSIteArr.clear();
        mdbhelper.open();
        Cursor csite = mdbhelper.getAllDatecUniqSite();
        if (null != csite) {

            if (csite.getCount() == 0) {
                iniqueSIteArr.add("null");
            } else {
                iniqueSIteArr.add("null");
                csite.moveToFirst();
                do {
                    iniqueSIteArr.add(csite.getString(0));
                } while (csite.moveToNext());
            }

        } else {

            iniqueSIteArr.add("null");
        }
        mdbhelper.close();
    }



    public void getAllCreaters() {
        createrList.clear();
        mdbhelper.open();
        Cursor csite = mdbhelper.getallCreaterUniq();

        if (null != csite) {

            if (csite.getCount() == 0)
            {
                createrList.add("All");
            }
            else
            {
                createrList.add("All");
                csite.moveToFirst();
                do
                {
                    createrList.add(csite.getString(0));
                }
                while (csite.moveToNext());

            }

        } else {
            createrList.add("All");
        }
        mdbhelper.close();
    }


    View.OnClickListener mClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnback:
                    finish();
                    break;
                case R.id.home:
                    if(Constants.isNetworkAvailable(context))
                        finish();
                    else
                        Constants.ShowToast(context,"Please  check your internet connection and try again");
                    break;
            }
        }
    };


    private void noDataDisplay()
    {
        quoteMonthList.clear();
        adapter.notifyDataSetChanged();
        nodata.setVisibility(View.VISIBLE);
        site_inspection_tv.setText("Quote Inspection Report (0)");
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_FIRST_USER)
        {
            setResult(RESULT_FIRST_USER);
            this.finish();
            return;
        }
        switch (requestCode)
        {
            case 5555: {
                if (resultCode == RESULT_OK)
                {
                    String url = Constants.GLOBAL_URL_NEW+"action=tempqutation";
                    mFilterModel = data.getExtras().getParcelable("fm");
                    // showDialog(typeBar);
                    if (null == mFilterModel.SiteName || mFilterModel.SiteName.equals("null"))
                    {
                    }
                    else
                    {
                        mdbhelper.open();
                        Cursor cr = mdbhelper.getallSiteID(mFilterModel.SiteName);
                        cr.moveToFirst();
                        url=url+"&site_id="+cr.getString(0);
                        mdbhelper.close();
                    }

                    if (mFilterModel.CreateName.equals("null"))
                    {
                    }
                    else
                    {
                        mdbhelper.open();
                        Cursor cr1 = mdbhelper.getallUserIds(mFilterModel.CreateName);
                        cr1.moveToFirst();
                        url=url+"&created_by="+cr1.getString(0);
                        mdbhelper.close();
                    }
                    if (null != mFilterModel.startDate && !mFilterModel.startDate.equals("")) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                        Date myDate = null;
                        try {
                            myDate = dateFormat.parse(mFilterModel.startDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String finalDate = timeFormat.format(myDate);
                        url=url+"&start_date="+finalDate;
                    }

                    if (null != mFilterModel.endDate && !mFilterModel.endDate.equals("")) {

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                        Date myDate = null;
                        try {
                            myDate = dateFormat.parse(mFilterModel.endDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String finalDate = timeFormat.format(myDate);
                        url=url+"&end_date="+finalDate;
                    }

                    mdbhelper.close();
                    Log.e("Himanshu","Dixit Request Url"+url);
                    if(Constants.isNetworkAvailable(context))
                    {
                        new GetQuotationList().execute(url);
                    }
                    else
                        Constants.ShowNetworkError(context);

                }
            }
            break;
            case 12345: {
                if (resultCode == RESULT_OK)
                {
                    if(Constants.isNetworkAvailable(context))
                    {
                        String url = Constants.GLOBAL_URL_NEW+"action=tempqutation";
                        new GetQuotationList().execute(url);
                    }
                    else
                        Constants.ShowNetworkError(context);
                }
            }
            break;
            default:
            break;
        }

    }



    private class GetQuotationList extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.callJsnGet(params[0]);
            return resp;
        }

        protected void onPostExecute(String resp)
        {
            Constants.hideProgressDialog();
            if (resp!=null)
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(resp);
                    JSONArray res = jsonObject.getJSONArray("W");
                    String status = res.getJSONObject(0).getString("Status");
                    if (status.equals("false"))
                    {
                        noDataDisplay();
                    }
                    else if (status.equals("true") && res.length()>1)
                    {
                        nodata.setVisibility(View.GONE);
                        quoteMonthList.clear();
                        JSONObject joData=res.optJSONObject(1);
                        if(joData!=null)
                        {
                            JSONObject joMonth=joData.optJSONObject("Month");
                            if(joMonth!=null)
                            {
                                ArrayList<QuoteMonthModel> tempList = (new JsonParser().parseQuotationMonthWise(joMonth));
                                if(tempList!=null && tempList.size()>0)
                                {
                                    quoteMonthList.addAll(tempList);
                                    adapter.notifyDataSetChanged();
                                    int totalCount=0;
                                    for(int i=0;i<tempList.size();i++)
                                    {
                                        if(tempList.get(i).getQuoteList()!=null && tempList.get(i).getQuoteList().size()>0)
                                            totalCount=totalCount+tempList.get(i).getQuoteList().size();
                                        if(tempList.get(i).getMonthName().equalsIgnoreCase(Constants.getMonthYear()))
                                            expandibleListView.expandGroup(i);
                                    }
                                    site_inspection_tv.setText("Quotations (" + totalCount + ")");
                                }
                            }
                            else
                                noDataDisplay();
                        }
                        else
                            noDataDisplay();
                    }
                    else
                    {
                        noDataDisplay();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else
                noDataDisplay();
        }
    }
}
