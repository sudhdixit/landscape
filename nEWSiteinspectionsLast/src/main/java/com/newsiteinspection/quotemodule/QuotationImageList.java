package com.newsiteinspection.quotemodule;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.multiplepicker.ImageModel1;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.apputility.GlobalAlertDialogs;
import com.newsiteinspection.apputility.HttpCall;
import com.newsiteinspection.helper.ImageLoader;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.utility.Adapter;

public class QuotationImageList extends Activity implements RequestReceiver {
    Button btnback = null, home = null;
    String selectedsites = "";
    RelativeLayout mid1 = null;
    Button addphoto = null, browsephoto = null;
    TextView username = null, textheader = null;
    ListView mListView = null;
    ProgressDialog progDialog;
    int typeBar = 0;
    int count;
    private String strusername;
    ArrayList<ImageModel1> mListMain;
    private NetReachability nr;
    private Context context;
    private String site_id,user_id,last_updated_date,filter_status;
    private MyCustomAdapter myadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.particularquotation_list);
        context=this;
        initView();
        setListner();
        getIntentData();
    }

    private void getIntentData()
    {
        site_id=getIntent().getStringExtra("site_id");
        last_updated_date=getIntent().getStringExtra("last_updated_date");
        user_id=getIntent().getStringExtra("user_id");
        /*filter_status=getIntent().getStringExtra("filter_status");
        if(TextUtils.isEmpty(filter_status))
            filter_status="1";*/
        filter_status="1";
        callListAPI();
    }

    public void callListAPI()
    {
        if (nr.isInternetOn())
        {
            mListMain.clear();
            myadapter.setList(mListMain);
            flagService = 4;
            showDialog(0);
            WebserviceHelper helper = new WebserviceHelper(this,Constants.GLOBAL_URL_NEW);
            helper.addParam("action", "tempquotationlist");
            helper.addParam("site_id",site_id);
            helper.addParam("last_update_date",last_updated_date);
            helper.addParam("user_id",user_id);
            helper.addParam("filter_status",filter_status);
            helper.execute(helper.buildUrl());
        }
        else
        {
            networkPopup();
        }

    }

    private void initView()
    {
        // TODO Auto-generated method stub
        home = (Button) findViewById(R.id.home);
        btnback = (Button) findViewById(R.id.btnback);
        addphoto = (Button) findViewById(R.id.addphoto);
        browsephoto = (Button) findViewById(R.id.browsephoto);
        nr = new NetReachability(this);
        username = (TextView) findViewById(R.id.username);
        SharedPreferences prefs = QuotationImageList.this.getSharedPreferences(
                "mobno", 0);
        strusername = prefs.getString("username", "");
        username.setText("Welcome " + strusername + " !");
        textheader = (TextView) findViewById(R.id.textheader);
        mid1 = (RelativeLayout) findViewById(R.id.mid1);
        mListView = (ListView) findViewById(R.id.inspectionsitelist);
        mListMain = new ArrayList<ImageModel1>();
        myadapter = new MyCustomAdapter(this, mListMain);
        mListView.setAdapter(myadapter);
    }

    @Override
    public void onBackPressed()
    {
        setResult(RESULT_OK);
        finish();
    }

    private void setListner()
    {
        btnback.setOnClickListener(laylistner);
        addphoto.setOnClickListener(laylistner);
        browsephoto.setOnClickListener(laylistner);
        home.setOnClickListener(laylistner);
    }

    OnClickListener laylistner = new OnClickListener() {

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            switch (arg0.getId()) {

                case R.id.btnback: {
                    onBackPressed();
                }
                break;
                case R.id.home:
                {
                    Intent i=new Intent(context,Home.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;

                case R.id.addphoto:
                {
                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(QuotationImageList.this,"Please  check your internet connection and try again",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {

                        Intent i = new Intent(QuotationImageList.this,AddNewQuotation1.class);
                        i.putExtra("site_id", site_id);
                        i.putExtra("last_updated_date", last_updated_date);
                        i.putExtra("created_by", user_id);
                        if(mListMain!=null && mListMain.size()>0)
                            i.putExtra("title", mListMain.get(0).title);
                        startActivityForResult(i,Constants.REQUEST_ADD_IMAGE_QUOTE);
                    }
                }
                break;
                default:
                    break;
            }
        }
    };
    private int flagService;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                progDialog = new ProgressDialog(this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.setCancelable(false);
                return progDialog;
            default:
                return null;
        }
    }

    // my adapter class using show list
    public class MyCustomAdapter extends BaseAdapter {
        ImageLoader imgLoder = null;
        private Context context;
        int posii;
        LayoutInflater li;
        ArrayList<ImageModel1> listMain;

        public MyCustomAdapter(Context _context, ArrayList<ImageModel1> list) {
            listMain = list;
            context = _context;
            this.li = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            imgLoder = new ImageLoader(QuotationImageList.this);
        }

        public int getCount() {
            return null == listMain ? 0 : listMain.size();
        }

        public ImageModel1 getItem(int position) {
            return listMain.get(position);
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public void setList(ArrayList<ImageModel1> list) {

            listMain = list;
            this.notifyDataSetChanged();
        }

        public View getView(final int position, View view, ViewGroup parent) {

            LayoutInflater inflate = ((Activity) context).getLayoutInflater();
            view = inflate.inflate(R.layout.quote_list_item, null);
            ImageView detail_arrow = (ImageView) view.findViewById(R.id.detail_arrow);
            ImageView siteimage = (ImageView) view.findViewById(R.id.siteimage);
            ImageButton deleteitem = (ImageButton) view.findViewById(R.id.deleteitem);
            final ImageButton selectitem = (ImageButton) view.findViewById(R.id.selectitem);
            final TextView sitedescription = (TextView) view.findViewById(R.id.sitedescription);
            final TextView amount = (TextView) view.findViewById(R.id.amount);
            final TextView status = (TextView) view.findViewById(R.id.status);
            final ImageView arrowup = (ImageView) view.findViewById(R.id.arrowup);
            final ImageView arrowdown = (ImageView) view.findViewById(R.id.arrowdown);
            arrowup.setId(position);
            arrowdown.setId(position);
            if(listMain.size()==1)
            {
                arrowup.setVisibility(View.GONE);
                arrowdown.setVisibility(View.GONE);
            }
            else
            {
                if(position==0)
                {
                    arrowup.setVisibility(View.GONE);
                    arrowdown.setVisibility(View.VISIBLE);
                }
                else if(position==(listMain.size()-1))
                {
                    arrowdown.setVisibility(View.GONE);
                    arrowup.setVisibility(View.VISIBLE);
                }
            }

            final ImageModel1 model = getItem(position);
            if(!TextUtils.isEmpty(model.remark))
                amount.setText("$"+model.remark);
            else
                amount.setText("$0");
            if(!TextUtils.isEmpty(model.status)&&model.status.equalsIgnoreCase("1"))
            {
                status.setBackgroundResource(R.drawable.rounded_button_green);
                status.setText("Active");
            }
            else
            {
                status.setBackgroundResource(R.drawable.rounded_button_red);
                status.setText("Inactive");
            }
            sitedescription.setText(model.description);
            imgLoder.DisplayImage("http://chenwa.biz/webservices/imagesuploaded/" + model.image_name, siteimage);

            arrowup.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {

                        String ids="";
                        int id=view.getId();
                        for(int i=0;i<listMain.size();i++)
                        {
                            if(i==(id-1))
                                ids=ids+listMain.get(id).id+",";
                            else if(i==id)
                                ids=ids+listMain.get(id-1).id+",";
                            else
                                ids=ids+listMain.get(i).id+",";
                        }
                        if(!TextUtils.isEmpty(ids))
                        {
                            ids = ids.substring(0,ids.length()-1);
                            showDialog(typeBar);
                            flagService = 3;
                            WebserviceHelper helper = new WebserviceHelper(QuotationImageList.this, Constants.GLOBAL_URL);
                            helper.addParam("action", "updateQuotationListOrder");
                            helper.addParam("quotationIds",ids);
                            helper.execute(helper.buildUrl());
                        }
                    }
                }
            });

            arrowdown.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        String ids="";
                        int id=view.getId();
                        for(int i=0;i<listMain.size();i++)
                        {
                            if(i==(id+1))
                                ids=ids+listMain.get(id).id+",";
                            else if(i==id)
                                ids=ids+listMain.get(id+1).id+",";
                            else
                                ids=ids+listMain.get(i).id+",";
                        }
                        if(!TextUtils.isEmpty(ids))
                        {
                            ids = ids.substring(0,ids.length()-1);
                            showDialog(typeBar);
                            flagService = 3;
                            WebserviceHelper helper = new WebserviceHelper(QuotationImageList.this, Constants.GLOBAL_URL);
                            helper.addParam("action", "updateQuotationListOrder");
                            helper.addParam("quotationIds",ids);
                            helper.execute(helper.buildUrl());
                        }
                    }
                }
            });
            status.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        showDialog(typeBar);
                        flagService =2;
                        WebserviceHelper helper = new WebserviceHelper(QuotationImageList.this,Constants.GLOBAL_URL);
                        helper.addParam("action","updateQuotationStatus");
                        helper.addParam("quotationId", model.id);
                        if(!TextUtils.isEmpty(model.status)&&model.status.equalsIgnoreCase("1"))
                            helper.addParam("status", "0");
                        else
                            helper.addParam("status", "1");
                        helper.execute(helper.buildUrl());
                    }
                }
            });
            siteimage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        startActivityForResult(NewEditQuotation.getIntent(QuotationImageList.this, model),22222);
                    }
                }
            });
            deleteitem.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {

                        final Dialog dialog = new Dialog(
                                QuotationImageList.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setContentView(R.layout.confirmation);
                        TextView txtTitle = (TextView) dialog
                                .findViewById(R.id.txtTitle);
                        TextView txtdescription = (TextView) dialog
                                .findViewById(R.id.txtdescription);
                        txtTitle.setText("Delete Photo");
                        txtdescription
                                .setText("Are you sure you want to delete this Photo?");
                        final Button btncancel = (Button) dialog
                                .findViewById(R.id.btncancel);
                        final Button btnDone = (Button) dialog
                                .findViewById(R.id.btnDone);
                        btncancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                // Auto-generated method stub

                                dialog.dismiss();
                            }
                        });
                        btnDone.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                dialog.dismiss();
                                showDialog(typeBar);
                                flagService = 1;
                                WebserviceHelper helper = new WebserviceHelper(QuotationImageList.this,Constants.GLOBAL_URL);
                                RemovedPos = position;
                                helper.addParam("action","deleteQuotation");
                                helper.addParam("inspection_id", model.id);
                                helper.execute(helper.buildUrl());
                            }
                        });
                        dialog.show();
                    }

                }
            });
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(NewEditQuotation.getIntent(
                                QuotationImageList.this, model), 22222);
                    }

                }
            });
            selectitem.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            QuotationImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                QuotationImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(NewEditQuotation.getIntent(
                                QuotationImageList.this, model), 22222);
                    }

                }

            });
            return view;
        }

    }

    public void networkPopup() {
        // this intent class using network checked
        Intent i = new Intent(QuotationImageList.this, NetworkPopup.class);
        startActivity(i);

    }

    @Override
    public void requestFinished(String result) {
        // TODO Auto-generated method stub
        try {
            dismissDialog(typeBar);
            switch (flagService) {
                case 1:
                {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray res = jsonObject.getJSONArray("W");
                    String status = res.getJSONObject(0).getString("Status");
                    if (status.equals("false"))
                    {

                    }
                    else if (status.equals("true"))
                    {
                        mListMain.remove(RemovedPos);
                        myadapter.setList(mListMain);
                        if (myadapter.getCount() == 0) {
                            setResult(RESULT_OK);
                            QuotationImageList.this.finish();
                            return;
                        }
                        setResult(RESULT_OK);
                    }
                }
                break;
                case 2:
                {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject res = jsonObject.optJSONObject("W");
                    if(res!=null)
                    {
                        String status = res.optString("status", "false");
                        if (status.equals("false"))
                        {
                            GlobalAlertDialogs.createAlertSingle((Activity) context,"Error in update status of Quote.", "OK", false);
                        } else if (status.equals("true")) {
                            callListAPI();
                            setResult(RESULT_OK);
                        }
                    }
                    else
                        GlobalAlertDialogs.createAlertSingle((Activity) context,"Error in update status of Quote.", "OK", false);
                }
                break;
                case 3:
                {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject res = jsonObject.optJSONObject("W");
                    if(res!=null)
                    {
                        String status = res.optString("status", "false");
                        if (status.equals("false"))
                        {
                            GlobalAlertDialogs.createAlertSingle((Activity) context,"Error in update order of Quote.", "OK", false);
                        } else if (status.equals("true")) {
                            callListAPI();
                            setResult(RESULT_OK);
                        }
                    }
                    else
                        GlobalAlertDialogs.createAlertSingle((Activity) context,"Error in update order of Quote.", "OK", false);
                }
                break;
                case 4:
                {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray res = jsonObject.getJSONArray("W");
                    String status = res.getJSONObject(0).getString("Status");
                    if (status.equals("false")) {

                    } else if (status.equals("true")) {
                        mListMain.clear();
                        mListMain = ImageModel1.getList(res.getJSONObject(1)
                                .getJSONArray("image_detail"));
                        myadapter.setList(mListMain);
                    }

                }
                break;
                default:
                    break;
            }

        } catch (JSONException e) {
            // TODO: handle exception
            progDialog.dismiss();
        }
    }

    int RemovedPos = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_FIRST_USER) {
            setResult(RESULT_FIRST_USER);
            this.finish();
            return;
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_ADD_IMAGE_QUOTE) {
            setResult(RESULT_OK);
            filter_status="1";
            callListAPI();
        }
        if (resultCode == RESULT_OK && requestCode == 22222)
        {
            callListAPI();
            setResult(RESULT_OK);
        }
    }
}
