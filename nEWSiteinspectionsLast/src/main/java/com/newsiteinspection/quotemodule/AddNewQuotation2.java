 package com.newsiteinspection.quotemodule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.ImageUpload;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.Receiver;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;

public class AddNewQuotation2 extends Activity implements RequestReceiver, Receiver
{
	private Context context;
	private int typeBar = 0;
	EditText txtdescription = null, txtremarks = null;
	Button nextsecond = null;
	Button btnback = null;
	Button home = null;
	ProgressDialog progDialog;
	private int year, month, day, hour, minute, second;
	String currentDateTime = null;
	private int flagService;
	String imagepath,site_id,site_name,title,created_by,last_updated_date;
	private String imgName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.addnewquotation2);
		context=this;
		imagepath=getIntent().getStringExtra("imagepath");
		site_id=getIntent().getStringExtra("site_id");
		site_name=getIntent().getStringExtra("site_name");
		title=getIntent().getStringExtra("title");
		created_by=getIntent().getStringExtra("created_by");
		last_updated_date=getIntent().getStringExtra("last_updated_date");
		if(TextUtils.isEmpty(created_by))
			created_by=AppUtil.getUserId(context);
		if(TextUtils.isEmpty(last_updated_date))
			last_updated_date=getDATE();
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		initView();
		setListnenr();
	}

	private void initView() {
		// TODO Auto-generated method stub
		btnback = (Button) findViewById(R.id.btnback);
		TextView username = (TextView) findViewById(R.id.username);
		home = (Button) findViewById(R.id.home);
		username.setText("Welcome " + AppUtil.getUserName(context) + " !");
		txtdescription = (EditText) findViewById(R.id.txtdescription);
		txtremarks = (EditText) findViewById(R.id.txtremarks);
		nextsecond = (Button) findViewById(R.id.nextsecond);

	}

	@SuppressWarnings("deprecation")
	private void initData()
	{
		// TODO Auto-generated method stub
		String strdescription = txtdescription.getText().toString();
		String strremark = txtremarks.getText().toString();
		NetReachability net = new NetReachability(AddNewQuotation2.this);
		boolean netcheck = net.isInternetOn();
		if (netcheck == false)
		{
			Constants.ShowToast(context,"Please check your network connection");
		}
		else
		{
			flagService = 1;
			if (!TextUtils.isEmpty(imagepath))
			{
				imgName = AppUtil.getUserId(context) + "_" + getSeparatedDate() + ".jpg";
				System.out.println("globle path =" + imgName);
				ImageUpload upload1 = new ImageUpload(AddNewQuotation2.this);
				upload1.execute(imagepath,imgName,"image");
			}
			if (imgName == "")
				imgName = "noimage.gif";
			showDialog(typeBar);
			WebserviceHelper helper = new WebserviceHelper(AddNewQuotation2.this, Constants.GLOBAL_URL);
			helper.addParam("action", "tempaddquotation");
			helper.addParam("site", site_id);
			helper.addParam("image_name",imgName);
			helper.addParam("description",strdescription);
			helper.addParam("remark", strremark);
			helper.addParam("title",title);
			helper.addParam("new_inspection_id","");
			helper.addParam("created_by",created_by);
			helper.addParam("user_id",AppUtil.getUserId(context));
			helper.addParam("last_update_date",last_updated_date);
			helper.execute(helper.buildUrl());
		}
	}


	private String getSeparatedDate()
	{
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		second = c.get(Calendar.SECOND);
		currentDateTime = DateFormate();
		// data separate string array
		String[] strdatetime = currentDateTime.split(" ");
		String date = strdatetime[0];
		String time = strdatetime[1];
		String[] strdate = date.split("-");
		String yy = strdate[0];
		String mmm = strdate[1];
		String dd = strdate[2];
		String[] strtime = time.split(":");
		String hh = strtime[0];
		String mm = strtime[1];
		String ss = strtime[2];
		String seprateDate = yy + mmm + dd + "_" + hh + mm + ss;
		return seprateDate;
	}

	public String getDATE() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(c.getTime());
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent i = new Intent();
		setResult(RESULT_CANCELED,i);
		this.finish();
	}

	public String DateFormate() {

		// set selected date into date_picker also
		if (day <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append("0").append(day)
					.append(" ").append(hour).append(":").append(minute)
					.append(":").append(second).toString();
		}
		if (month <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append("0").append(month + 1).append("-").append(day)
					.append(" ").append(hour).append(" ").append(":")
					.append(minute).append(":").append(second).toString();
		}
		if (day <= 9 && month <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append("0").append(month + 1).append("-").append("0")
					.append(day).append(" ").append(hour).append(":")
					.append(minute).append(":").append(second).toString();
		} else {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append(day).append(" ")
					.append(hour).append(":").append(minute).append(":")
					.append(second).toString();
		}

		System.out.println("get currentDateTime =" + currentDateTime);
		return currentDateTime;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			progDialog = new ProgressDialog(this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Please wait...");
			progDialog.setCanceledOnTouchOutside(false);
			progDialog.setCancelable(true);
			return progDialog;
		default:
			return null;
		}
	}

	private void setListnenr() {
		// TODO Auto-generated method stub
		home.setOnClickListener(laylistner);
		nextsecond.setOnClickListener(laylistner);
		btnback.setOnClickListener(laylistner);
	}

	OnClickListener laylistner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.nextsecond:
				try {
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getCurrentFocus()
							.getWindowToken(), 0);
				} catch (Exception e) {

				}
				initData();
				break;
			case R.id.btnback: {
				onBackPressed();
			    }
				break;
			case R.id.home:
			{
				Intent i=new Intent(context,Home.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
			default:
				break;
			}
		}
	};

	public void networkPopup() {
		// this intent class using network checked
		Intent i = new Intent(AddNewQuotation2.this, NetworkPopup.class);
		startActivity(i);

	}

	@Override
	public void requestFinished(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(result);
			JSONArray res = jsonObject.getJSONArray("W");
			String status = res.getJSONObject(0).getString("Status");
			if (flagService == 1)
			{
				if (status.equals("false")) {
					progDialog.dismiss();
					Intent i = new Intent(this, AlertPopupScreen.class);
					i.putExtra("utilityTitle", "Unable To Add site ...!");
					i.putExtra("utilityMessage","Please add site after sometime");
					startActivity(i);
				}
				else if (status.equals("true"))
				{
					Intent in = new Intent();
					in.putExtra("site_id",site_id);
					in.putExtra("last_updated_date",last_updated_date);
					in.putExtra("user_id",created_by);
					setResult(RESULT_OK,in);
					this.finish();
				}
			}
		} catch (JSONException e) {
			// TODO: handle exception
			progDialog.dismiss();
		} catch (Exception e) {
			// TODO: handle exception
			progDialog.dismiss();
			networkPopup();
		}
	}

	@Override
	public void requestFinish(String result) {
		// TODO Auto-generated method stub

	}
}
