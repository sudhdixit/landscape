package com.newsiteinspection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.inspectionmodule.Addnewsite;
import com.newsiteinspection.inspectionmodule.InspectionImageList;
import com.newsiteinspection.inspectionmodule.InspectionSiteList;
import com.newsiteinspection.quotemodule.AddNewQuotation1;
import com.newsiteinspection.quotemodule.QuotationImageList;
import com.newsiteinspection.quotemodule.QuotesListingActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends Activity implements RequestReceiver{
    LinearLayout logout = null, admin_wrapper = null, edit_site_inspection = null, edit_site_quotation = null,
            new_site_inspection = null,new_site_quotation;
    ImageView admin = null;
    ImageView add_quotation;
    ImageView edit_quotation;
    private TextView username;
    private String strusername, str_user_type;
    private String newPassword;

    ProgressDialog progDialog;
    private int flagService;
    Button btnChangePassword = null;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home);
        context=this;
        initView();
        // initData();
        setListner();

        startService(new Intent(this, TimeService.class));
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    private void initView() {
        // TODO Auto-generated method stub
        SharedPreferences prefs = null;
        admin = (ImageView) findViewById(R.id.admin);
        add_quotation = (ImageView) findViewById(R.id.add_quotation);
        edit_quotation = (ImageView) findViewById(R.id.edit_quotation);
        username = (TextView) findViewById(R.id.username);
        btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
        str_user_type = AppUtil.getUserType(context);
        username.setText("Welcome " + AppUtil.getUserName(context) + " !");
        logout = (LinearLayout) findViewById(R.id.logout);
        edit_site_quotation = (LinearLayout) findViewById(R.id.edit_site_quotation);
        admin_wrapper = (LinearLayout) findViewById(R.id.admin_wrapper);
        edit_site_inspection = (LinearLayout) findViewById(R.id.edit_site_inspection);
        new_site_inspection = (LinearLayout) findViewById(R.id.new_site_inspection);
        new_site_quotation= (LinearLayout) findViewById(R.id.new_site_quotation);

        if (str_user_type.equalsIgnoreCase("supervisor") || str_user_type.equalsIgnoreCase("admin"))
        {
            admin.setImageResource(R.drawable.icon_3);
        }
        else
        {
            admin.setImageResource(R.drawable.home_refresh_icon_grey);
        }

        if (str_user_type.equalsIgnoreCase("admin"))
        {
            add_quotation.setImageResource(R.drawable.new_quotation_icon);
            edit_quotation.setImageResource(R.drawable.edit_quotation_icon);
        }
        else
        {
            add_quotation.setImageResource(R.drawable.home_refresh_icon_grey);
            edit_quotation.setImageResource(R.drawable.home_refresh_icon_grey);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                System.out.println("heeeeee");
                progDialog = new ProgressDialog(this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.setCancelable(true);
                return progDialog;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        Home.this.finish();
    }

    private void setListner() {
        logout.setOnClickListener(laylistner);
        edit_site_inspection.setOnClickListener(laylistner);
        new_site_inspection.setOnClickListener(laylistner);
        admin_wrapper.setOnClickListener(laylistner);
        new_site_quotation.setOnClickListener(laylistner);
        edit_site_quotation.setOnClickListener(laylistner);
        btnChangePassword.setOnClickListener(laylistner);
    }

    Dialog dialog;
    OnClickListener laylistner = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.logout:
                {
                    Intent i = new Intent(Home.this, Login.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;
                case R.id.edit_site_inspection: {
                    NetReachability net = new NetReachability(Home.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                Home.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Intent i = new Intent(Home.this,InspectionSiteList.class);
                        startActivity(i);
                    }
                }
                break;
                case R.id.new_site_inspection:
                {
                    NetReachability net = new NetReachability(Home.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                Home.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Intent i = new Intent(Home.this, Addnewsite.class);
                        startActivityForResult(i, Constants.REQUEST_ADD_SITE_1);
                    }
                }
                break;
                case R.id.edit_site_quotation:
                {
                    if (str_user_type.equalsIgnoreCase("admin"))
                    {
                        NetReachability net = new NetReachability(Home.this);
                        boolean netcheck = net.isInternetOn();
                        if (netcheck == false) {
                            Toast.makeText(
                                    Home.this,
                                    "Please  check your internet connection and try again",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Intent i = new Intent(Home.this, QuotesListingActivity.class);
                            startActivity(i);
                        }
                    }
                }
                break;
                case R.id.new_site_quotation:
                {
                    if (str_user_type.equalsIgnoreCase("admin"))
                    {
                        NetReachability net = new NetReachability(Home.this);
                        boolean netcheck = net.isInternetOn();
                        if (netcheck == false) {
                            Toast.makeText(
                                    Home.this,
                                    "Please  check your internet connection and try again",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Intent i = new Intent(Home.this, AddNewQuotation1.class);
                            startActivityForResult(i, Constants.REQUEST_ADD_QUOTE_1);
                        }
                    }
                }
                break;
                case R.id.admin_wrapper:
                {
                    if (str_user_type.equalsIgnoreCase("supervisor") || str_user_type.equalsIgnoreCase("admin"))
                    {
                        SharedPreferences spf = Home.this.getSharedPreferences(
                                "mobno", 0);
                        String userName = spf.getString("checkusername", null);
                        String userPass = spf.getString("checkpassword", null);
                        String url = "";

                        http:
                        if (null != userName && null != userPass
                                && !userName.equals("") && !userPass.equals("")) {

                            url = "http://chenwa.biz/webservices/webservice.php?username="
                                    + userName
                                    + "&password="
                                    + userPass
                                    + "&action=login1";

                        }
                        else
                        {
                            return;
                        }

                        try {
                            Intent myIntent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(url));
                            startActivity(myIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "No application can handle this request."
                                            + " Please install a webbrowser",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }
                break;
                case R.id.btnChangePassword: {
                    change_password();
                }
                break;

            }
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == Constants.REQUEST_NEW_QUOTE_IMAGE)
        {
            Intent i = new Intent(Home.this, QuotesListingActivity.class);
            startActivity(i);
        }
        else if(requestCode == Constants.REQUEST_NEW_SITE_IMAGE)
        {
            Intent i = new Intent(Home.this, InspectionSiteList.class);
            startActivity(i);
        }
        else if (resultCode == RESULT_OK)
        {
            if (requestCode == Constants.REQUEST_ADD_QUOTE_1)
            {
                Intent i = new Intent(Home.this, QuotationImageList.class);
                i.putExtra("site_id", data.getStringExtra("site_id"));
                i.putExtra("last_updated_date", data.getStringExtra("last_updated_date"));
                i.putExtra("user_id",data.getStringExtra("user_id"));
                i.putExtra("filter_status","0");
                startActivityForResult(i, Constants.REQUEST_NEW_QUOTE_IMAGE);
            }
            if (requestCode == Constants.REQUEST_ADD_SITE_1)
            {
                Intent i = new Intent(Home.this, InspectionImageList.class);
                i.putExtra("site_id", data.getStringExtra("site_id"));
                i.putExtra("last_updated_date", data.getStringExtra("last_updated_date"));
                i.putExtra("user_id",data.getStringExtra("user_id"));
                i.putExtra("filter_status","0");
                startActivityForResult(i, Constants.REQUEST_NEW_SITE_IMAGE);
            }
            else if (requestCode == 111111 && resultCode == RESULT_OK)
            {
                if (null != data && data.getStringExtra("site_NEW") != null && !data.getStringExtra("site_NEW").equals(""))
                {
                    Intent i = new Intent(Home.this, InspectionSiteList.class);
                    i.putExtra("site_NEW", data.getStringExtra("site_NEW"));
                    startActivity(i);
                }
            }
        }

    }

    ;


    public void change_password() {
        dialog = new Dialog(Home.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.change_password);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Change Password !!");
        final Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        final Button btnDone = (Button) dialog.findViewById(R.id.btnDone);
        btncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        btnDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String oldPassword = null;
                String newPassword = null;
                String confirmPassword = null;

                final EditText oldPasswordET = (EditText) dialog.findViewById(R.id.etxtOldPassword);
                final EditText newPasswordET = (EditText) dialog.findViewById(R.id.etxtConfirmPassword);
                final EditText confirmPasswordET = (EditText) dialog.findViewById(R.id.etxtNewPassword);

                oldPassword = oldPasswordET.getText().toString().trim();
                newPassword = newPasswordET.getText().toString().trim();
                confirmPassword = confirmPasswordET.getText().toString().trim();

                if (validate(oldPassword, newPassword, confirmPassword)) {
                    dialog.dismiss();
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus()
                                .getWindowToken(), 0);
                    } catch (Exception e) {

                    }
                    serverPWDRequest(oldPassword, newPassword);
                }

            }

            private void serverPWDRequest(String oldPassword, String newPassword) {
                // TODO Auto-generated method stub
                NetReachability net = new NetReachability(Home.this);
                boolean netcheck = net.isInternetOn();
                if (netcheck == false) {
                    netCheckConfirmation_dialog();
                } else {
                    //lylLodingView.setVisibility(View.VISIBLE);
                    flagService = 3;
                    showDialog(0);
                    progDialog.show();
                    //progDialog.show();
                    SharedPreferences prefs = null;
                    prefs = Home.this.getSharedPreferences("mobno", 0);
                    Home.this.newPassword = newPassword;
                    WebserviceHelper helper = new WebserviceHelper(Home.this,Constants.GLOBAL_URL);
                    helper.addParam("action", "change_password");
                    helper.addParam("user_id", prefs.getString("uid", null));
                    helper.addParam("old_password", oldPassword);
                    helper.addParam("new_password", newPassword);
                    helper.execute(helper.buildUrl());
                }
            }
        });
        dialog.show();
    }


    private void netCheckConfirmation_dialog() {
        // TODO Auto-generated method stub

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);

        alertDialog
                .setMessage("No Internet Connection Available. Do you want to try again?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        NetReachability net = new NetReachability(Home.this);
                        boolean netcheck = net.isInternetOn();
                        if (netcheck == false) {
                            netCheckConfirmation_dialog();
                        } else {
                            dialog.dismiss();
                        }
                    }
                });

        alertDialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.dismiss();
                        Home.this.finish();
                    }
                });
        alertDialog.show();

    }


    public Boolean validate(String oldP, String newP, String confirmP) {
        Boolean status = false;

        if (oldP.length() > 0 && newP.length() > 0 && confirmP.length() > 0) {
            if (newP.equals(confirmP)) {
                SharedPreferences prefs = null;
                prefs = Home.this.getSharedPreferences("mobno", 0);
                String checkpass = prefs.getString("checkpassword", null);

                if (oldP.equals(checkpass)) {
                    status = true;
                } else {
                    status = false;
                    Toast.makeText(Home.this, "Old Password Wrong!!", Toast.LENGTH_LONG).show();
                }
            } else {
                status = false;
                Toast.makeText(Home.this, "ConfirmPassword not Match!!", Toast.LENGTH_LONG).show();
            }


        } else {
            status = false;
            Toast.makeText(Home.this, "Pls complete all fields!!", Toast.LENGTH_LONG).show();

        }
        return status;

    }

    @Override
    public void requestFinished(String result) {
        // TODO Auto-generated method stub
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray res = jsonObject.getJSONArray("W");
            String status = res.getJSONObject(0).getString("Status");
            if (flagService == 3) {
                progDialog.dismiss();

                if (status.equals("false")) { // result failed error show in

                    Toast.makeText(Home.this, "Password not Change!!", Toast.LENGTH_LONG).show();

                } else if (status.equals("true")) {

                    Toast.makeText(Home.this, "Password Change Successfully!!", Toast.LENGTH_LONG).show();
                    SharedPreferences prefs = null;
                    prefs = Home.this.getSharedPreferences("mobno", 0);
                    Editor edit = prefs.edit();
                    edit.putString("checkpassword", newPassword).commit();
                }
            }
        } catch (JSONException e) {
            // TODO: handle exception
            System.out.println(e.toString());
            progDialog.dismiss();
        }
    }
}
