package com.newsiteinspection.model;

import java.util.ArrayList;

/**
 * Created by Sudh on 9/18/2016.
 */
public class QuoteMonthModel
{
    private String monthName;
    private ArrayList<QuoteModel> quoteList;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public ArrayList<QuoteModel> getQuoteList() {
        return quoteList;
    }

    public void setQuoteList(ArrayList<QuoteModel> quoteList) {
        this.quoteList = quoteList;
    }
}
