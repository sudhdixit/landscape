package com.newsiteinspection.model;

import java.util.ArrayList;

/**
 * Created by Sudh on 9/18/2016.
 */
public class SiteMonthModel {
    private String monthName;
    private ArrayList<SiteModel> siteList;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public ArrayList<SiteModel> getSiteList() {
        return siteList;
    }

    public void setSiteList(ArrayList<SiteModel> siteList) {
        this.siteList = siteList;
    }
}


