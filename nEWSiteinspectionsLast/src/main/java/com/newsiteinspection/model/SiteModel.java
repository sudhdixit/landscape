package com.newsiteinspection.model;

import com.multiplepicker.ImageModel;
import com.multiplepicker.ImageModel1;

import java.util.ArrayList;

/**
 * Created by Sudh on 9/18/2016.
 */
public class SiteModel
{
    private String Month_Name;
    private String id;
    private String site_id;
    private String user_id;
    private String SiteName;
    private String LastUpdateDateTime;
    private String NumberOfPhotos;
    private String ViewReport;
    private String EmailReport;
    private String total_inspection_rerports;
    private String UserName;
    private ArrayList<ImageModel> imageList;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public ArrayList<ImageModel> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<ImageModel> imageList) {
        this.imageList = imageList;
    }

    public String getMonth_Name() {
        return Month_Name;
    }

    public void setMonth_Name(String month_Name) {
        Month_Name = month_Name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSiteName() {
        return SiteName;
    }

    public void setSiteName(String siteName) {
        SiteName = siteName;
    }

    public String getLastUpdateDateTime() {
        return LastUpdateDateTime;
    }

    public void setLastUpdateDateTime(String lastUpdateDateTime) {
        LastUpdateDateTime = lastUpdateDateTime;
    }

    public String getNumberOfPhotos() {
        return NumberOfPhotos;
    }

    public void setNumberOfPhotos(String numberOfPhotos) {
        NumberOfPhotos = numberOfPhotos;
    }

    public String getViewReport() {
        return ViewReport;
    }

    public void setViewReport(String viewReport) {
        ViewReport = viewReport;
    }

    public String getEmailReport() {
        return EmailReport;
    }

    public void setEmailReport(String emailReport) {
        EmailReport = emailReport;
    }

    public String getTotal_inspection_rerports() {
        return total_inspection_rerports;
    }

    public void setTotal_inspection_rerports(String total_inspection_rerports) {
        this.total_inspection_rerports = total_inspection_rerports;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SiteModel that = (SiteModel) o;
        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
