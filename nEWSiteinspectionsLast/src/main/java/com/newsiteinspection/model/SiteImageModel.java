package com.newsiteinspection.model;

/**
 * Created by Sudh on 9/18/2016.
 */
public class SiteImageModel
{
    private String id;
    private String user_id;
    private String site;
    private String title;
    private String image_name;
    private String description;
    private String remark;
    private String quotation_no;
    private String rivision;
    private String main_quotation_id;
    private String last_updated_date;
    private String updated_at;
    private String updated_by;
    private String new_inspection_id;
    private String status;
    private String ordersite;
    private String site_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getQuotation_no() {
        return quotation_no;
    }

    public void setQuotation_no(String quotation_no) {
        this.quotation_no = quotation_no;
    }

    public String getRivision() {
        return rivision;
    }

    public void setRivision(String rivision) {
        this.rivision = rivision;
    }

    public String getMain_quotation_id() {
        return main_quotation_id;
    }

    public void setMain_quotation_id(String main_quotation_id) {
        this.main_quotation_id = main_quotation_id;
    }

    public String getLast_updated_date() {
        return last_updated_date;
    }

    public void setLast_updated_date(String last_updated_date) {
        this.last_updated_date = last_updated_date;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getNew_inspection_id() {
        return new_inspection_id;
    }

    public void setNew_inspection_id(String new_inspection_id) {
        this.new_inspection_id = new_inspection_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrdersite() {
        return ordersite;
    }

    public void setOrdersite(String ordersite) {
        this.ordersite = ordersite;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }
}
