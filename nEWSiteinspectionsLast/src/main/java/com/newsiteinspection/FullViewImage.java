package com.newsiteinspection;

import java.io.IOException;

import com.multiplepicker.ImageLoaderUniversal;
import com.newsiteinspection.helper.Imageload;
import com.newsiteinspection.helper.TouchImageView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class FullViewImage extends Activity {

	TouchImageView touchImage;
	Button btnback;
	String headerName = null;
	TextView txtHeader = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.full_view_image);

		intiVeiws();
	}

	private void intiVeiws() {
		// TODO Auto-generated method stub

		touchImage = (TouchImageView) findViewById(R.id.touchImage);
		txtHeader = (TextView)findViewById(R.id.txtHeader);

		Intent i1 = getIntent();

		String frm = i1.getStringExtra("FRM");

		String link = i1.getStringExtra("URL");
		headerName = i1.getStringExtra("HeaderText");
		if (headerName!=null&&headerName.length()!=0) {
			txtHeader.setText(headerName);	
			txtHeader.setVisibility(View.VISIBLE);
		} else {
			txtHeader.setVisibility(View.INVISIBLE);
		}
			
		
		if (frm.equals("SERVER")) {

			Imageload imgLoder = new Imageload(FullViewImage.this);
			imgLoder.DisplayImage(link, touchImage);

		} else if (frm.equals("LOCAL")) {
			touchImage.setImageBitmap(getPreview(link));

		}

		
		ImageLoaderUniversal.ImageLoadSquare(this, link, touchImage, ImageLoaderUniversal.normal_Image_diplay_options);
		 
		btnback = (Button) findViewById(R.id.btnback);
		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FullViewImage.this.finish();
			}
		});

	}

	Bitmap getPreview(String uri) {
		Bitmap photo_camera = null;
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inSampleSize = 4;
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(uri, bounds);
		if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
			return null;

		int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
				: bounds.outWidth;

		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = originalSize / 200;
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(uri);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		int orientation = exif
				.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
		System.out.println("ORIIIIIIIIIIIIIIIIIIIIIIII =" + orientation);
		photo_camera = BitmapFactory.decodeFile(uri, opts);
		Matrix matrix = new Matrix();
		if ((orientation == 3)) {
			matrix.postRotate(180);
			// matrix.postScale(400/(float) outputBitmap.getWidth(),
			// 400/(float)outputBitmap.getHeight());
			photo_camera = Bitmap.createBitmap(photo_camera, 0, 0,
					photo_camera.getWidth(), photo_camera.getHeight(), matrix,
					true);
			return photo_camera;
		} else if (orientation == 6) {
			matrix.postRotate(90);
			// matrix.postScale(400/(float) outputBitmap.getWidth(),
			// 400/(float)outputBitmap.getHeight());
			photo_camera = Bitmap.createBitmap(photo_camera, 0, 0,
					photo_camera.getWidth(), photo_camera.getHeight(), matrix,
					true);
			return photo_camera;
		} else if (orientation == 8) {
			matrix.postRotate(270);
			// matrix.postScale(400/(float) outputBitmap.getWidth(),
			// 400/(float)outputBitmap.getHeight());
			photo_camera = Bitmap.createBitmap(photo_camera, 0, 0,
					photo_camera.getWidth(), photo_camera.getHeight(), matrix,
					true);
			return photo_camera;
		}

		return BitmapFactory.decodeFile(uri, opts);

	}
}
