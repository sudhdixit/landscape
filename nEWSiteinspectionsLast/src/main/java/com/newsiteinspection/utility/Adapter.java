
package com.newsiteinspection.utility;

import java.io.IOException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Adapter {
	protected static final String TAG = "DataAdapter";

	private final Context mContext;
	private SQLiteDatabase mDb;
	private DataBaseHelper mDbHelper;

	public Adapter(Context context) {
		this.mContext = context;
		mDbHelper = new DataBaseHelper(mContext);
	}

	public Adapter createDatabase() throws SQLException {
		try {
			mDbHelper.createDataBase();
		} catch (IOException mIOException) {
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
			throw new Error("UnableToCreateDatabase");
		}
		return this;
	}

	public Adapter open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDbHelper.close();
			mDb = mDbHelper.getWritableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		mDbHelper.close();
	}


	public Cursor getAllDatecUniqSite()
	{
		try
		{
			String sql = "SELECT DISTINCT site_name FROM site_list";
			Cursor mCur = mDb.rawQuery(sql, null);

			return mCur;
		} catch (SQLException mSQLException) {

			throw mSQLException;
		}
	}
	
	public Cursor getallCreaterUniq() {
		try {

			String sql = "SELECT DISTINCT staff_name FROM user";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;
		} catch (SQLException mSQLException) {

			throw mSQLException;
		}
	}
	
	
	public Cursor getallSiteID(String sitename) {
		try {

			String sql = "SELECT DISTINCT site_id FROM site_list WHERE site_name='"+sitename+"'";
			Cursor mCur = mDb.rawQuery(sql, null);

			return mCur;
		} catch (SQLException mSQLException) {

			throw mSQLException;
		}
	}

	public Cursor getSiteName(String siteId) {
		try {

			String sql = "SELECT DISTINCT site_name FROM site_list WHERE site_id='"+siteId+"'";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;
		} catch (SQLException mSQLException) {

			throw mSQLException;
		}
	}
	
	public Cursor getallUserIds(String userNames) {
		try {

			String sql = "SELECT DISTINCT user_id FROM user where staff_name='"+userNames+"'";
			Cursor mCur = mDb.rawQuery(sql, null);
			return mCur;
		} catch (SQLException mSQLException) {

			throw mSQLException;
		}
	}

	public boolean addsite(ContentValues cv) {

		try {
			mDb.insert("site_list", null, cv);
			return true;

		} catch (SQLException ex)
		{
			return false;
		}
	}

	public boolean addUser(ContentValues cv) {

		try {
			mDb.insert("user", null, cv);
			return true;

		} catch (SQLException ex) {

			return false;
		}
	}



	public Cursor getAllSite() {
		try {
			String sql = "SELECT * FROM site_list";
			Cursor mCur = mDb.rawQuery(sql, null);
			if (mCur != null) {
				mCur.moveToLast();
			}
			return mCur;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "getImageSave >>" + mSQLException.toString());
			throw mSQLException;
		}
	}
	public boolean deletesite() {
		try {
			String sql = "DELETE FROM site_list";
			mDb.execSQL(sql);
			System.out.println("deleteAllSate =" + sql);

			return true;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "notdeleteAllSate >>" + mSQLException.toString());
			return false;
		}
	}

	public boolean deleteUser() {
		try {
			String sql = "DELETE FROM user";
			mDb.execSQL(sql);
			System.out.println("deleteAllUser =" + sql);

			return true;
		} catch (SQLException mSQLException) {
			Log.e(TAG, "notdeleteAllSate >>" + mSQLException.toString());
			return false;
		}
	}
}
