package com.newsiteinspection.utility;

import java.util.ArrayList;

import com.newsiteinspection.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

public class FilterAdapter extends BaseAdapter {

	ArrayList<String> listMain = new ArrayList<String>();;
	LayoutInflater li;
	Context mContext;

	public FilterAdapter(Context c) {
		mContext = c;
		li = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void SetList(ArrayList<String> listMain1) {
		listMain = listMain1;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return null == listMain ? 0 : listMain.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return listMain.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = li.inflate(
				R.layout.item_filter_row, parent, false);

		CheckedTextView text1 = (CheckedTextView) convertView
				.findViewById(R.id.ch1);

		text1.setText(getItem(position));
		if (position == 0) {
			text1.setText("All Sites");
		}

		return convertView;
	}

}
