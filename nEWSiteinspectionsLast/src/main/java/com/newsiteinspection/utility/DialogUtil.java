package com.newsiteinspection.utility;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;

public class DialogUtil {
	public static void showListDialog(Context context, int titleResId,
			final OnListItemClickCustom onListItemClickListner,
			final ArrayList<String> lists) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (0 != titleResId) {
			builder.setTitle(titleResId);
		}
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
				context, android.R.layout.simple_list_item_1);
		arrayAdapter.addAll(lists);

		builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int position) {
				String item = arrayAdapter.getItem(position);
				if (null != onListItemClickListner) {
					onListItemClickListner.onItemClick(position, item);
				}

			}
		});

		AlertDialog dialog = builder.create();

		builder.show();
	}

}
