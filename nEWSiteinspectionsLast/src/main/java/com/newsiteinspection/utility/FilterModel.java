package com.newsiteinspection.utility;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterModel implements Parcelable {
	public String startDate;
	public String endDate;
	public String SiteName = "null";

	public String CreateName = "null";

	
	public FilterModel() {

	}

	public void SetstartDate(String startDat) {
		this.startDate = startDat;
	}

	public void SetendDate(String endDate) {
		this.endDate = endDate;
	}

	public void SetendSiteName(String SiteName) {
		this.SiteName = SiteName;
	}
	
	public void SetendCreateName(String creter) {
		this.CreateName = creter;
	}

	public FilterModel(Parcel in) {
		startDate = in.readString();
		endDate = in.readString();
		SiteName = in.readString();
		CreateName = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(startDate);
		dest.writeString(endDate);
		dest.writeString(SiteName);


		dest.writeString(CreateName);

		
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public FilterModel createFromParcel(Parcel in) {
			return new FilterModel(in);
		}

		public FilterModel[] newArray(int size) {
			return new FilterModel[size];
		}
	};

	@Override
	public String toString() {
		return "CATEGORY [  id=" + ", category=" + ", gender=" + "]";
	}

}
