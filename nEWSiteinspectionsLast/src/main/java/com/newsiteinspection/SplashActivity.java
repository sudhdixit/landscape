package com.newsiteinspection;

import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.GlobalVariable;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.utility.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SplashActivity extends Activity implements RequestReceiver{

	private static final int SPLASH_TIME = 2000;
	SharedPreferences prefs = null;
	RelativeLayout lylLodingView=null;
	Editor editor = null;
	ProgressDialog progDialog;
	String uid="";
	private int flagService;
	private Adapter mdbhelper ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_site_inspection);
		lylLodingView=(RelativeLayout)findViewById(R.id.lylLodingView);
		getUser();
		mdbhelper = new Adapter(SplashActivity.this);
		mdbhelper.createDatabase();
		/*try {
			new Handler().postDelayed(new Runnable() {

				public void run() {
						Intent in = new Intent(SplashActivity.this, Login.class);
						finish();
						startActivity(in);
				}

			}, SPLASH_TIME);
		} catch (Exception e) {
		}*/
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void getUser() {
		// TODO Auto-generated method stub
		Log.e("Himanshu", "Dixit Splash Get User Called");
		flagService = 1;
		WebserviceHelper helper = new WebserviceHelper(SplashActivity.this,Constants.GLOBAL_URL_NEW);
		helper.addParam("action","getStaff");
		helper.execute(helper.buildUrl());
	}


	@Override
	public void requestFinished(String result) {
		// TODO Auto-generated method stub
		try {
			if (flagService == 1)
			{
				if(result!=null)
				{
					JSONObject jsonObject = new JSONObject(result);
					JSONArray res = jsonObject.getJSONArray("W");
					String status = res.getJSONObject(0).getString("Status");
					if (status.equals("false"))
					{
						Intent i = new Intent(this, AlertPopupScreen.class);
						i.putExtra("utilityTitle", "Login Error");
						i.putExtra("utilityMessage",
								"error while connecting server");
						startActivity(i);
					}
					else if (status.equals("true"))
					{
						mdbhelper.open();
						mdbhelper.deleteUser();
						for (int i1 = 1; i1 < res.length(); i1++)
						{
							JSONObject json_data1 = res.getJSONObject(i1);
							ContentValues cv = new ContentValues();
							cv.put("user_id", json_data1.getString("id"));
							cv.put("staff_name",json_data1.getString("staff_name"));
							mdbhelper.addUser(cv);
						}
						mdbhelper.close();
						Intent i = new Intent(SplashActivity.this,Login.class);
						startActivity(i);
						finish();
					}
				}
				else
				{
					networkPopup();
				}
			}
		}
		catch (JSONException e) {
			// TODO: handle exception
			networkPopup();
		}
		catch (Exception e) {
			// TODO: handle exception
			networkPopup();
		}
	}

	public void networkPopup() {
		Intent i = new Intent(SplashActivity.this, NetworkPopup.class);
		startActivity(i);
	}
}
