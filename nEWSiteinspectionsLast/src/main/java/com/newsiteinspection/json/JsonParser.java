package com.newsiteinspection.json;

import com.multiplepicker.ImageModel;
import com.multiplepicker.ImageModel1;
import com.newsiteinspection.model.QuoteModel;
import com.newsiteinspection.model.QuoteMonthModel;
import com.newsiteinspection.model.SiteModel;
import com.newsiteinspection.model.SiteMonthModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;
public class JsonParser
{
    public ArrayList<QuoteMonthModel> parseQuotationMonthWise(JSONObject joData) {
        ArrayList<QuoteMonthModel> quoteList = new ArrayList<>();
        if (joData != null) {
            try {
                Iterator iterator = joData.keys();
                while (iterator.hasNext())
                {
                    QuoteMonthModel model = new QuoteMonthModel();
                    String key = (String)iterator.next();
                    model.setMonthName(key);
                    JSONArray joArray = joData.optJSONArray(key);
                    if (joArray != null && joArray.length() > 0)
                    {
                        ArrayList<QuoteModel> quoteModelList = new ArrayList<>();
                        for (int i = 0; i < joArray.length(); i++)
                        {
                            JSONArray joChildArray = joArray.optJSONArray(i);
                            if (joChildArray != null && joChildArray instanceof JSONArray)
                            {
                                for (int j = 0; j < joChildArray.length(); j++) {
                                    JSONObject joChildObject = joChildArray.optJSONObject(j);
                                    if (joChildObject != null)
                                    {
                                        QuoteModel quoteModel = new QuoteModel();
                                        quoteModel.setMonth_Name(getStringFromJSON(joChildObject, "Month_Name"));
                                        quoteModel.setId(getStringFromJSON(joChildObject, "id"));
                                        quoteModel.setSite_id(getStringFromJSON(joChildObject, "site_id"));
                                        quoteModel.setUser_id(getStringFromJSON(joChildObject, "user_id"));
                                        quoteModel.setSiteName(getStringFromJSON(joChildObject, "SiteName"));
                                        quoteModel.setUserName(getStringFromJSON(joChildObject, "UserName"));
                                        quoteModel.setLastUpdateDateTime(getStringFromJSON(joChildObject, "LastUpdateDateTime"));
                                        quoteModel.setNumberOfPhotos(getStringFromJSON(joChildObject, "NumberOfPhotos"));
                                        quoteModel.setViewReport(getStringFromJSON(joChildObject, "ViewReport"));
                                        quoteModel.setEmailReport(getStringFromJSON(joChildObject, "EmailReport"));
                                        quoteModel.setTotal_inspection_rerports(getStringFromJSON(joChildObject, "total_inspection_rerports"));
                                        JSONArray joChildImageArray = joChildObject.optJSONArray("image_detail");
                                        if (joChildImageArray != null && joChildImageArray.length() > 0)
                                        {
                                            for (int k = 0; k < joChildImageArray.length(); k++)
                                            {
                                                ArrayList<ImageModel1> imageList = new ArrayList<>();
                                                JSONObject joChildImageObj = joChildImageArray.optJSONObject(k);
                                                if (joChildImageObj != null)
                                                {
                                                    ImageModel1 modelImage=new ImageModel1(joChildImageObj);
                                                    imageList.add(modelImage);
                                                }
                                                quoteModel.setImageList(imageList);
                                            }
                                        }
                                        quoteModelList.add(quoteModel);
                                    }
                                }
                            }
                        }
                        model.setQuoteList(quoteModelList);
                    }
                    quoteList.add(model);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return quoteList;
    }



    public ArrayList<SiteMonthModel> parseSiteMonthWise(JSONObject joData) {
        ArrayList<SiteMonthModel> quoteList = new ArrayList<>();
        if (joData != null) {
            try {
                Iterator iterator = joData.keys();
                while (iterator.hasNext())
                {
                    SiteMonthModel model = new SiteMonthModel();
                    String key = (String) iterator.next();
                    model.setMonthName(key);
                    JSONArray joArray = joData.optJSONArray(key);
                    if (joArray != null && joArray.length() > 0) {
                        ArrayList<SiteModel> quoteModelList = new ArrayList<>();
                        for (int i = 0; i < joArray.length(); i++)
                        {
                            JSONArray joChildArray = joArray.optJSONArray(i);
                            if (joChildArray != null && joChildArray instanceof JSONArray)
                            {
                                for (int j = 0; j < joChildArray.length(); j++) {
                                    JSONObject joChildObject = joChildArray.optJSONObject(j);
                                    if (joChildObject != null)
                                    {
                                        SiteModel quoteModel = new SiteModel();
                                        quoteModel.setMonth_Name(getStringFromJSON(joChildObject, "Month_Name"));
                                        quoteModel.setId(getStringFromJSON(joChildObject, "id"));
                                        quoteModel.setSite_id(getStringFromJSON(joChildObject, "site_id"));
                                        quoteModel.setUser_id(getStringFromJSON(joChildObject, "user_id"));
                                        quoteModel.setSiteName(getStringFromJSON(joChildObject, "SiteName"));
                                        quoteModel.setUserName(getStringFromJSON(joChildObject, "UserName"));
                                        quoteModel.setLastUpdateDateTime(getStringFromJSON(joChildObject, "LastUpdateDateTime"));
                                        quoteModel.setNumberOfPhotos(getStringFromJSON(joChildObject, "NumberOfPhotos"));
                                        quoteModel.setViewReport(getStringFromJSON(joChildObject, "ViewReport"));
                                        quoteModel.setEmailReport(getStringFromJSON(joChildObject, "EmailReport"));
                                        quoteModel.setTotal_inspection_rerports(getStringFromJSON(joChildObject, "total_inspection_rerports"));
                                        JSONArray joChildImageArray = joChildObject.optJSONArray("image_detail");
                                        if (joChildImageArray != null && joChildImageArray.length() > 0)
                                        {
                                            for (int k = 0; k < joChildImageArray.length(); k++)
                                            {
                                                ArrayList<ImageModel> imageList = new ArrayList<>();
                                                JSONObject joChildImageObj = joChildImageArray.optJSONObject(k);
                                                if (joChildImageObj != null)
                                                {
                                                    ImageModel modelImage=new ImageModel(joChildImageObj);
                                                    imageList.add(modelImage);
                                                }
                                                quoteModel.setImageList(imageList);
                                            }
                                        }
                                        quoteModelList.add(quoteModel);
                                    }
                                }
                            }
                        }
                        model.setSiteList(quoteModelList);
                    }
                    quoteList.add(model);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return quoteList;
    }


    public String getStringFromJSON(JSONObject json, String key) {
        String value = ""; // Blank string by default.
        try {
            value = json.getString(key);
            if (value.equals("null") || value == null)
                value = "";
            return value;
        } catch (JSONException exp) {
            exp.getMessage();
        }
        return value;  // this wil return BLANk string if object is not prasent.
    }


}
