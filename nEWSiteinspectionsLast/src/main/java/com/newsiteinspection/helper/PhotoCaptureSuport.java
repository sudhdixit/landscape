package com.newsiteinspection.helper;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;

@SuppressLint("NewApi")
public class PhotoCaptureSuport {

	private Bitmap Image=null,ImageCreateIssue=null;
	private String cat=null,catid=null;
	private String ImagePath=null,videoPath=null,ImagePathCreateissue=null;
	
	private static PhotoCaptureSuport instance; 
	    public static PhotoCaptureSuport getInstance() {
	        if (instance == null) {
	            instance = new PhotoCaptureSuport();
	        }
	        return instance;
	    }

	    public String getCat() {
			return cat;
		}

		public void setCat(String cat) {
			this.cat = cat;
		}

		public String getCatid() {
			return catid;
		}

		public void setCatid(String catid) {
			this.catid = catid;
		}

		public Bitmap getImage() {
	       return this.Image;
	    }
	    public Bitmap getImageCreateIssue() {
		       return this.ImageCreateIssue;
		    }
	    
	    public String getVideoPath() {
		       return this.videoPath;
	    }
	    public void setVideoPath(String imgPath1) 
	    {
	    	 this.ImagePathCreateissue =null;
	    	 this.videoPath =imgPath1;
	    }
	    public String getPath() {
		       return this.ImagePath;
	    }
	    public String getPathCraeteIssue() {
		       return this.ImagePathCreateissue;
	    }
	    public void setImageCreateIssue(String imgPath) 
	    {    ImageCreateIssue=null;
	         ImagePathCreateissue="";
	    
	    	
	    	   this.ImagePathCreateissue =imgPath;
	    	   this.ImageCreateIssue = picture(ImagePathCreateissue);
	    	
	    }
	    public void setImage(String imgPath2) 
	    {
	    	if(imgPath2 == null) {
	    	   this.ImagePath =imgPath2;
	    	} else if(imgPath2.length() > 0) {
	    	   this.ImagePath =imgPath2;
	    	   this.Image = picture(imgPath2);
	    	 }
	    }
	    
	   public Bitmap picture(String imgPath12)  {

		   // Bitmap bitmap=null;
		    Bitmap outputBitmap=null;
		    Bitmap photo_camera=null;
			BitmapFactory.Options op = new BitmapFactory.Options();
			op.inSampleSize = 16;
			photo_camera = BitmapFactory.decodeFile(imgPath12, op);			
			System.out.println("photo_camera ====="+photo_camera.getWidth()+"===="+photo_camera.getHeight());
			
			photo_camera = Bitmap.createScaledBitmap(photo_camera,photo_camera.getWidth(),photo_camera.getWidth(), true);
			outputBitmap = Bitmap.createBitmap(photo_camera.getWidth(),photo_camera.getHeight(), Config.ARGB_8888);
			
			Canvas canvas = new Canvas(outputBitmap);
			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, photo_camera.getWidth(), photo_camera.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = 4;
			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(photo_camera, rect, rect, paint);
			ExifInterface exif = null;
			try {
				exif = new ExifInterface(imgPath12);
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
            
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
	        System.out.println("ORIIIIIIIIIIIIIIIIIIIIIIII ="+orientation);
	        
	        Matrix matrix = new Matrix();
	         if ((orientation == 3)) {
	             matrix.postRotate(180);
//	             matrix.postScale(400/(float) outputBitmap.getWidth(), 400/(float)outputBitmap.getHeight());
	             photo_camera = Bitmap.createBitmap(outputBitmap, 0, 0,outputBitmap.getWidth(), outputBitmap.getHeight(), matrix, true);
	             return photo_camera;
	         } else if (orientation == 6) {
	             matrix.postRotate(90);
//	             matrix.postScale(400/(float) outputBitmap.getWidth(), 400/(float)outputBitmap.getHeight());
	             photo_camera = Bitmap.createBitmap(outputBitmap, 0, 0, outputBitmap.getWidth(),outputBitmap.getHeight(), matrix, true);
	             return photo_camera;
	         }
	         else if (orientation == 8) {
	             matrix.postRotate(270);
//	             matrix.postScale(400/(float) outputBitmap.getWidth(), 400/(float)outputBitmap.getHeight());
             photo_camera = Bitmap.createBitmap(outputBitmap, 0, 0, outputBitmap.getWidth(),outputBitmap.getHeight(),matrix, true);
	             return photo_camera;
	         }
	      
		   return outputBitmap;
	   }    
}
