/**
 * this interface exists just to allow the WebserviceHelper to make callbacks.
 */
package com.newsiteinspection.helper;

public interface Receiver {
	void requestFinish(String result);
	
 }
