package com.newsiteinspection.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.newsiteinspection.apputility.Constants;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GlobalVariable extends Application {

    // Variables to maintain Registration Reference person.
    public static String strReferenceName = "Sudhanshu";
    //	 public static String deviceImeiNo="352954061990281";
    public static String deviceImeiNo = "357376051138736";
    public static ArrayList<HashMap<String, String>> arrLatlong = new ArrayList<HashMap<String, String>>();
    public static ArrayList<HashMap<String, String>> arrsiteinspection = new ArrayList<HashMap<String, String>>();

    public void change(HashMap<String, String> s, int pos) {
        arrsiteinspection.set(pos, s);
    }

    public ArrayList<HashMap<String, String>> getLatLong() {
        return arrLatlong;
    }

    @Override
    public void onCreate() {
        if (Constants.Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
        }
        super.onCreate();
        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }


    public void clears() {
        arrLatlong.clear();
        arrsiteinspection.clear();
    }

    public void remove(int pos, int pos1) {

        arrsiteinspection.remove(pos);

    }

    public void removeinspection(int pos) {


        arrsiteinspection.remove(pos);
    }

    public void additem(HashMap<String, String> s, HashMap<String, String> s1, int pos) {
        arrsiteinspection.add(s);
        arrLatlong.set(pos, s1);

    }

    public void editsites(HashMap<String, String> s, int pos) {

        arrLatlong.set(pos, s);

    }

    public void setLatLong(ArrayList<HashMap<String, String>> latlong) {
        arrLatlong = new ArrayList<HashMap<String, String>>();
        arrLatlong = latlong;
    }

    public ArrayList<HashMap<String, String>> getTypes() {
        return arrsiteinspection;
    }

    public void setTypes(ArrayList<HashMap<String, String>> specilistlist) {
        arrsiteinspection = new ArrayList<HashMap<String, String>>();
        arrsiteinspection = specilistlist;
    }


}
