/**
 * this interface exists just to allow the WebserviceHelper to make callback.
 */
package com.newsiteinspection.helper;

public interface RequestReceiver {
	void requestFinished(String result);
}
