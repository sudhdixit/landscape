package com.newsiteinspection.helper;

import com.newsiteinspection.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class AlertPopupScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alert_popup);

		Intent intent = getIntent();
		String title = intent.getStringExtra("utilityTitle");
		String showSms = intent.getStringExtra("utilityMessage");	
		TextView txtsms = (TextView) findViewById(R.id.txtmsg);
		TextView txtutilitytitle = (TextView) findViewById(R.id.txtutilitytitle);
		txtutilitytitle.setText(title);
		txtsms.setText(showSms);
		
	}

	public void onClickDismis(View v) {
		this.finish();
	}
}
