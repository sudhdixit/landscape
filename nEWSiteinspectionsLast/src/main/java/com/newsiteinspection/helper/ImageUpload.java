package com.newsiteinspection.helper;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class ImageUpload extends AsyncTask<String, Void, String> {

	public ImageUpload(Receiver context) {
		mContext = context;
	}

	private Receiver mContext;
	HttpURLConnection connection = null;
	DataOutputStream outputStream = null;
	DataInputStream inputStream = null;

	String urlServer = "";
	
	String lineEnd = "\r\n";
	String twoHyphens = "--";
	String boundary = "*****";

	int bytesRead, bytesAvailable, bufferSize;
	byte[] buffer;
	int maxBufferSize = 5 * 1024 * 1024;
	int serverResponseCode;
	String serverResponseMessage = "";
	File imgPathFile=null;
	@Override
	protected String doInBackground(String... Path) {

		try {
           
			String pathToOurFile = Path[0];
			String filename = Path[1];
			String flage=Path[2];
            if(flage.equals("image")) 
            {
            	//urlServer ="http://poha.co.in/webservice/imagesuploaded/new_uploader.php";
            	urlServer = "http://chenwa.biz/webservices/uploader.php";
            	System.out.println("ssiiiiiiizeee  "+new File(pathToOurFile).length());
            	if(new File(pathToOurFile).length()>600*1000){
                	imgPathFile=PicturePath(pathToOurFile);
                	}
                	else
                	{
                		imgPathFile=new File(pathToOurFile);	
                	}
            }
            else
            {
            	//urlServer ="http://poha.co.in/webservice/imagesuploaded/videouploader.php";
            	urlServer = "http://chenwa.biz/webservices/videouploader.php";
            	imgPathFile=new File(pathToOurFile);
            }
			FileInputStream fileInputStream = new FileInputStream(imgPathFile);
			URL url = new URL(urlServer);
			System.out.println("urlsss"+url);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type","multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded\";filename=\""+ filename + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens+ lineEnd);

			System.out.println(outputStream);
			// Responses from the server (code and message)
			serverResponseCode = connection.getResponseCode();
			serverResponseMessage = connection.getResponseMessage();

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();

		}

		catch (Exception e) {
			Log.e("Waseef", "webservice exception: =" + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("serverResponseMessage ="+serverResponseMessage+"  "+serverResponseCode);
		return serverResponseMessage;
	}

	@Override
	protected void onPostExecute(String result) {
		((Receiver) mContext).requestFinish(result);
	}

	public void execute(StringBuilder s) {
	}
	
	public File PicturePath(String imagePath) {
		
    	    System.out.println("imageName make in name ="+imagePath);
		    Bitmap bitmap=null;
	        BitmapFactory.Options bitoption = new BitmapFactory.Options();
	        bitoption.inSampleSize = 8;
			Bitmap bitmapPhoto = BitmapFactory.decodeFile(imagePath,bitoption);

			bitmap = Bitmap.createScaledBitmap(bitmapPhoto,bitmapPhoto.getWidth(),bitmapPhoto.getHeight(), true);
			ExifInterface exif = null;
			try {
				exif = new ExifInterface(imagePath);
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
	        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
	        System.out.println("ORIIIIIIIIIIIIIIIIIIIIIIII ="+orientation);
	        Matrix matrix = new Matrix();
	        if ((orientation == 3)) {
	            matrix.postRotate(180);
//	            matrix.postScale((float) bitmapPhoto.getWidth(), (float)bitmapPhoto.getHeight());
	           bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0,bitmapPhoto.getWidth(), bitmapPhoto.getHeight(), matrix, true);
	          
	        } else if (orientation == 6) {
	        	matrix.postRotate(90);
//	            matrix.postScale((float) bitmapPhoto.getWidth(), (float)bitmapPhoto.getHeight());
	            bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0, bitmapPhoto.getWidth(),bitmapPhoto.getHeight(), matrix, true);
	           
	        }
	        else if (orientation == 8) {
	            matrix.postRotate(270);
//	            matrix.postScale((float) bitmapPhoto.getWidth(),(float)bitmapPhoto.getHeight());
	            bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0, bitmapPhoto.getWidth(),bitmapPhoto.getHeight(),matrix, true);
	        
	        }
	        
			//float aspectRatio = (float) bitmapPhoto.getWidth() / (float) bitmapPhoto.getHeight();
			//int newHeight = (int) (1100 / aspectRatio);
	      
			
		    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		    File myDirectory = new File(Environment.getExternalStorageDirectory(),"inspections");
			   if (!myDirectory.exists()) {
				   myDirectory.mkdirs();
			    }
			 String  imgName = String.valueOf(System.currentTimeMillis()) + ".jpg";
		    File file = new File(Environment.getExternalStorageDirectory(),"inspections/"+imgName);  
		    System.out.println("compress start file ="+file);
	     
	        try {
	            file.createNewFile();
	            FileOutputStream fo = new FileOutputStream(file);
	            fo.write(bytes.toByteArray());
	            fo.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        System.out.println("compress End file ="+file);
		return file;

	}	
}
