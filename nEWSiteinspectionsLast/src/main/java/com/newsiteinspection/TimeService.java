package com.newsiteinspection;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings.Secure;
import android.widget.Toast;

import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;

public class TimeService extends Service {
	// constant
	public static final long NOTIFY_INTERVAL = 60 * 1000; // 10 seconds

	// run on another Thread to avoid crash
	private Handler mHandler = new Handler();
	// timer handling
	private Timer mTimer = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);

		mLocationTracker.startUsingLocation(1, mLocationListener);

		if (mLocationTracker.canGetLocation()) {
			mCurrentLocation = mLocationTracker.getLastKnownLocation(false);

		}
	}

	public boolean stopService(Intent name) {
		// TODO Auto-generated method stub
		mTimeDisplayTimerTask.cancel();
		return super.stopService(name);
	}

	TimeDisplayTimerTask mTimeDisplayTimerTask;

	@Override
	public void onCreate() {
		// cancel if already existed
		if (mTimer != null) {
			mTimer.cancel();
		} else {
			// recreate new
			mTimer = new Timer();
		}
		mTimeDisplayTimerTask = new TimeDisplayTimerTask();
		// schedule task
		mTimer.scheduleAtFixedRate(mTimeDisplayTimerTask, 0, NOTIFY_INTERVAL);
		mLocationTracker = new LocationTracker(getApplicationContext());

	}

	class TimeDisplayTimerTask extends TimerTask {

		@Override
		public void run() {
			// run on another thread
			mHandler.post(new Runnable() {

				@Override
				public void run() {
					// display toast

					if (null != mCurrentLocation) {

						System.out.println("LOCATION"
								+ mCurrentLocation.getLatitude());
					} else {
						System.out.println("LOCATION null");
					}

					SharedPreferences prefs = getApplicationContext()
							.getSharedPreferences("mobno", 0);
					String status = prefs.getString("LOGINSTATUS", null);
					String uid = prefs.getString("uid", null);

					String android_id = Secure.getString(
							getApplicationContext().getContentResolver(),
							Secure.ANDROID_ID);

					NetReachability nr = new NetReachability(
							getApplicationContext());

					if (nr.isInternetOn()
							&& mLocationTracker.isLocationenable()
							&& null != mCurrentLocation && null != status) {
						//
						// http://chenwa.biz/webservices/webservice.php?action=addLocation&user_id=30&device_id=yut78&http://chenwa.biz/webservices/webservice.php?action=addLocation&user_id=30&device_id=yut78&lat=18.92488028662047&long=18.92488028662047=18.92488028662047&long=18.92488028662047
						WebserviceHelper helper = new WebserviceHelper(ed, Constants.GLOBAL_URL);

						helper.addParam("action", "addLocation");
						helper.addParam("user_id", uid);
						helper.addParam("device_id", android_id);
						helper.addParam("lat",
								"" + mCurrentLocation.getLatitude());
						helper.addParam("long",
								"" + mCurrentLocation.getLongitude());

						helper.execute(helper.buildUrl());

					}

				}

			});
		}

		private String getDateTime() {
			// get date time in custom format
			SimpleDateFormat sdf = new SimpleDateFormat(
					"[yyyy/MM/dd - HH:mm:ss]");
			return sdf.format(new Date());
		}

	}

	private Location mCurrentLocation;
	private LocationTracker mLocationTracker;

	private RequestReceiver ed = new RequestReceiver() {

		@Override
		public void requestFinished(String result) {
			// TODO Auto-generated method stub

		}
	};
	private LocationListener mLocationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
			mCurrentLocation = location;
			if (null != mCurrentLocation) {
			}
		}
	};
}
