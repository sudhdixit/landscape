package com.newsiteinspection;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.util.Log;

public class LocationTracker {
	private static String TAG = LocationTracker.class.getSimpleName();
	public static final String DEFAULT_PROVIDER = "LocationTracker";
	private Context activity;
	// flag for GPS status
	public boolean isGPSEnabled = false;
	// flag for network status
	private boolean isNetworkEnabled = false;
	// flag for GPS status
	private boolean started = false, canGetLocation = false;

	private Location lastKnownLocation; // location

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 15*60*1000;

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public LocationTracker(Context activity) {
		this.activity = activity;
	}

	/**
	 * Function to get the user's current location
	 *
	 * @return
	 */
	public void startUsingLocation(final int updateCount,
			final LocationListener locationListener) {
		if (!started) {
			try {
				locationManager = (LocationManager) activity
						.getSystemService(Context.LOCATION_SERVICE);

				// getting GPS status
				isGPSEnabled = locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
				Log.v(TAG, "startUsingLocation - isGPSEnabled=" + isGPSEnabled);
				// getting network status
				isNetworkEnabled = locationManager
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				Log.v(TAG, "startUsingLocation - isNetworkEnabled="
						+ isNetworkEnabled);

				if (!isGPSEnabled && !isNetworkEnabled) {
					this.canGetLocation = false;
					// Intent viewIntent = new
					// Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					// activity.startActivity(viewIntent);
				} else {
					this.canGetLocation = true;
					if (isNetworkEnabled) {
						lastKnownLocation = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (null != locationListener) {
							if (updateCount == 1) {
								locationManager.requestSingleUpdate(
										LocationManager.NETWORK_PROVIDER,
										locationListener,
										Looper.getMainLooper());
							} else {
								locationManager.requestLocationUpdates(
										LocationManager.NETWORK_PROVIDER,
										MIN_TIME_BW_UPDATES,
										MIN_DISTANCE_CHANGE_FOR_UPDATES,
										locationListener);
							}
						}
						started = true;
					}
					// if GPS Enabled get lat/long using GPS Services
					if (isGPSEnabled) {
						lastKnownLocation = locationManager
								.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (null != locationListener) {
							if (updateCount == 1) {
								locationManager.requestSingleUpdate(
										LocationManager.GPS_PROVIDER,
										locationListener,
										Looper.getMainLooper());
							} else {
								locationManager.requestLocationUpdates(
										LocationManager.GPS_PROVIDER,
										MIN_TIME_BW_UPDATES,
										MIN_DISTANCE_CHANGE_FOR_UPDATES,
										locationListener);
							}
						}
						started = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				started = false;
			}
		}
	}

	public boolean isLocationenable() {
		boolean isGPSEnabled = false;
		try {
			LocationManager locationManager = (LocationManager) activity
					.getSystemService(Context.LOCATION_SERVICE);
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception e) {
			isGPSEnabled = false;
		}
		return isGPSEnabled;
	}

	public Location getLastKnownLocation(boolean synthesize) {
		if (synthesize && null == lastKnownLocation) {
			return new Location(DEFAULT_PROVIDER);
		}
		return lastKnownLocation;
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 */
	public void stopUsingLocation(LocationListener locationListener) {
		if (started && locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}
		started = false;
	}

	/**
	 * Function to check GPS/wifi enabled
	 *
	 * @return boolean
	 */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * lauch Settings Options
	 */
	// public void showSettingsAlert(final Activity activity, int msgResId) {
	// final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	// builder.setMessage(activity.getString(msgResId > 0 ? msgResId :
	// R.string.Enable_Location_Msg)).setCancelable(false)
	// .setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
	// public void onClick(@SuppressWarnings("unused") final DialogInterface
	// dialog, @SuppressWarnings("unused") final int id) {
	// activity.startActivity(new
	// Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	// }
	// })
	// .setNegativeButton(android.R.string.cancel, new
	// DialogInterface.OnClickListener() {
	// public void onClick(final DialogInterface dialog,
	// @SuppressWarnings("unused") final int id) {
	// activity.finish();
	// }
	// });
	//
	// final AlertDialog alert = builder.create();
	// alert.show();
	// }
}