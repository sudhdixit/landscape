package com.newsiteinspection.apputility;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class AppUtil 
{
	public static String SHARED_PREFERENCE="mobno";
	
	public static void setUserId(Context c, String user_id) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("uid", user_id);
		editor.commit();
	}

	public static String getUserId(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("uid", "");
	}


	public static void setUserName(Context c, String username) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("username", username);
		editor.commit();
	}

	public static String getUserName(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("username", "");
	}

	public static void setUserEmail(Context c, String email) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("email", email);
		editor.commit();
	}

	public static String getUserEmail(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("email", "");
	}


	public static void setPassword(Context c, String password) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("password", password);
		editor.commit();
	}

	public static String getPassword(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("password", "");
	}

	public static void setLoginStatus(Context c, String LOGINSTATUS) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("LOGINSTATUS", LOGINSTATUS);
		editor.commit();
	}

	public static String getLoginStatus(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("LOGINSTATUS", "");
	}


	public static void setUserType(Context c, String user_type) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("user_type", user_type);
		editor.commit();
	}

	public static String getUserType(Context c) {
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		return sp.getString("user_type", "");
	}

	public static boolean isLoggedIn(Context c)
	{
		SharedPreferences sp = c.getSharedPreferences(SHARED_PREFERENCE, 0);
		if(!TextUtils.isEmpty(sp.getString("user_id","")))
		{
			return true;
		}
		else
			return false;
			
	}
}
