package com.newsiteinspection.apputility;

import android.util.Log;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class HttpCall {

    String res = null;

    public String callJsnGet(String urlString) {
        Log.e("Himanshu", "Dixit Call Url" + urlString);
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        Log.e("Himanshu", "Dixit Response String" + chaine.toString());
        return chaine.toString();

    }


    public String callJsnPost(String urlString) {
        Log.e("Himanshu", "Dixit Call Url" + urlString);
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        return chaine.toString();
    }

    public String callJsnDelete(String urlString) {
        StringBuffer chaine = new StringBuffer("");
        try {
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("DELETE");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
            return null;
        }
        return chaine.toString();

    }



    public String executeHttpPostRequest(String server_url, String data) {
        String result = "";
        try {
            URL url = new URL(server_url.replaceAll(" ", "%20"));
            Log.e("Himanshu", "Dixit Api Url" + url);
            URLConnection connection = url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setConnectTimeout(Constants.DEFAULT_HTTP_CONNECT_TIMEOUT);
            connection.setReadTimeout(Constants.DEFAULT_HTTP_READ_TIMEOUT);
            // Send the POST data
            DataOutputStream dataOut = new DataOutputStream(connection.getOutputStream());
            dataOut.writeBytes(data);
            dataOut.flush();
            dataOut.close();

            // get the response from the server and store it in result
            DataInputStream dataIn = new DataInputStream(connection.getInputStream());
            String inputLine;
            while ((inputLine = dataIn.readLine()) != null) {
                result += inputLine;
            }
            dataIn.close();
        } catch (IOException e) {
            e.printStackTrace();
            result = null;
        }
        Log.e("Himanshu","Dixit Api Response"+result);
        return result;
    }








    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
