package com.newsiteinspection.apputility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.newsiteinspection.R;

public class GlobalAlertDialogs {


    public static void createAlertDouble(final Context context, String message, String confirmBtn, final boolean finish) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog);
        TextView msg = (TextView) dialog.findViewById(R.id.txtdescription);
        msg.setText(message);
        Button btnCancel = (Button) dialog.findViewById(R.id.btncancel);
        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnDone);
        btnConfirm.setText(confirmBtn);
        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (finish)
                    ((Activity) context).finish();
            }
        });
        dialog.show();
    }


    public static void createAlertSingle(final Activity mActivity, String message, String confirmBtn, final boolean finish) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog_single);
        TextView msg = (TextView) dialog.findViewById(R.id.txtdescription);
        msg.setText(message);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnDone);
        btnConfirm.setText(confirmBtn);
        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (finish)
                    mActivity.finish();
            }
        });
        dialog.show();
    }
}
