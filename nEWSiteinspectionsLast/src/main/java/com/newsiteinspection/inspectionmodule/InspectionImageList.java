package com.newsiteinspection.inspectionmodule;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.multiplepicker.ImageModel;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.apputility.HttpCall;
import com.newsiteinspection.helper.ImageLoader;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;

public class InspectionImageList extends Activity implements RequestReceiver {
    Button btnback = null, home = null;
    String selectedsites = "";
    RelativeLayout mid1 = null;
    Button addphoto = null, browsephoto = null;
    TextView username = null, textheader = null;
    ListView mListView = null;
    ProgressDialog progDialog;
    int typeBar = 0;
    int count;
    ArrayList<ImageModel> mListMain;
    private NetReachability nr;
    private Context context;
    private String site_id,user_id,last_updated_date,filter_status;
    private MyCustomAdapter myadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.particularsite_list);
        context=this;
        initView();
        setListner();
        getIntentData();
    }

    private void getIntentData() {

        site_id=getIntent().getStringExtra("site_id");
        last_updated_date=getIntent().getStringExtra("last_updated_date");
        user_id=getIntent().getStringExtra("user_id");
        filter_status=getIntent().getStringExtra("filter_status");
        if(TextUtils.isEmpty(filter_status))
            filter_status="1";
        callListAPI();
    }

    public void callListAPI()
    {
        if (nr.isInternetOn()) {
            mListMain.clear();
            myadapter.setList(mListMain);
            flagService = 4;
            showDialog(0);
            WebserviceHelper helper = new WebserviceHelper(this,Constants.GLOBAL_URL_NEW);
            helper.addParam("action","tempinspectionlist");
            helper.addParam("site_id", site_id);
            helper.addParam("last_update_date",last_updated_date);
            helper.addParam("user_id",user_id);
            helper.addParam("filter_status",filter_status);
            helper.execute(helper.buildUrl());
        }
        else
        {
            networkPopup();
        }
    }

    private void initView() {
        // TODO Auto-generated method stub
        home = (Button) findViewById(R.id.home);
        btnback = (Button) findViewById(R.id.btnback);
        addphoto = (Button) findViewById(R.id.addphoto);
        browsephoto = (Button) findViewById(R.id.browsephoto);
        nr = new NetReachability(this);
        username = (TextView) findViewById(R.id.username);
        username.setText("Welcome " + AppUtil.getUserName(context) + " !");
        textheader = (TextView) findViewById(R.id.textheader);
        mid1 = (RelativeLayout) findViewById(R.id.mid1);
        mListView = (ListView) findViewById(R.id.inspectionsitelist);
        mListMain = new ArrayList<ImageModel>();
        myadapter = new MyCustomAdapter(this, mListMain);
        mListView.setAdapter(myadapter);
    }

    @Override
    public void onBackPressed()
    {
        setResult(RESULT_OK);
        finish();
    }

    private void setListner() {
        // TODO Auto-generated method stub
        btnback.setOnClickListener(laylistner);
        addphoto.setOnClickListener(laylistner);
        browsephoto.setOnClickListener(laylistner);
        home.setOnClickListener(laylistner);
    }

    OnClickListener laylistner = new OnClickListener() {

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            switch (arg0.getId()) {

                case R.id.btnback: {
                    onBackPressed();
                }
                break;
                case R.id.home:
                {
                    Intent i=new Intent(context,Home.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;
                case R.id.addphoto: {
                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Intent i = new Intent(InspectionImageList.this,Addnewsite.class);
                        i.putExtra("site_id", site_id);
                        i.putExtra("last_updated_date", last_updated_date);
                        i.putExtra("created_by", user_id);
                        startActivityForResult(i,Constants.REQUEST_ADD_IMAGE_SITE);
                    }
                }
                break;
                default:
                    break;
            }
        }
    };
    private int flagService;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                progDialog = new ProgressDialog(this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.setCancelable(false);
                return progDialog;
            default:
                return null;
        }
    }

    // my adapter class using show list
    public class MyCustomAdapter extends BaseAdapter {
        ImageLoader imgLoder = null;
        private Context context;
        int posii;
        LayoutInflater li;
        ArrayList<ImageModel> listMain;

        public MyCustomAdapter(Context _context, ArrayList<ImageModel> list) {
            listMain = list;
            context = _context;
            this.li = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            imgLoder = new ImageLoader(InspectionImageList.this);
        }

        public int getCount() {
            return null == listMain ? 0 : listMain.size();
        }

        public ImageModel getItem(int position) {
            return listMain.get(position);
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public void setList(ArrayList<ImageModel> list) {

            listMain = list;
            this.notifyDataSetChanged();
        }

        public View getView(final int position, View view, ViewGroup parent) {

            LayoutInflater inflate = ((Activity) context).getLayoutInflater();
            view = inflate.inflate(R.layout.site_list_item, null);
            ImageView detail_arrow = (ImageView) view
                    .findViewById(R.id.detail_arrow);
            ImageButton editimage = (ImageButton) view
                    .findViewById(R.id.editimage);
            editimage.setVisibility(View.GONE);
            ImageView siteimage = (ImageView) view.findViewById(R.id.siteimage);
            ImageButton deleteitem = (ImageButton) view
                    .findViewById(R.id.deleteitem);
            CheckBox repeatChkBx = (CheckBox) view.findViewById(R.id.checkbox1);
            final TextView txtDone = (TextView) view.findViewById(R.id.txt_Done);

            final ImageButton selectitem = (ImageButton) view.findViewById(R.id.selectitem);

            final TextView sitedescription = (TextView) view.findViewById(R.id.sitedescription);
            final ImageModel model = getItem(position);
            sitedescription.setText(model.description);
            imgLoder.DisplayImage("http://chenwa.biz/webservices/imagesuploaded/" + model.image_name, siteimage);

            if(!TextUtils.isEmpty(model.checked) && model.checked.equals("1")) {
                repeatChkBx.setChecked(true);
                txtDone.setText("Done");
            }
            else {
                repeatChkBx.setChecked(false);
                txtDone.setText("");
            }
            repeatChkBx.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url=Constants.GLOBAL_URL_NEW+"action=changeInspectionChecked&inspection_id="+model.id;
                    new CheckedTask().execute(url);
                }
            });
            siteimage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(NewEditinspection.getIntent(InspectionImageList.this, model),22222);
                    }


                }
            });
            editimage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else
                    {
                        startActivityForResult(NewEditinspection.getIntent(InspectionImageList.this, model), 22222);
                    }
                }
            });
            deleteitem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {

                        final Dialog dialog = new Dialog(
                                InspectionImageList.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setContentView(R.layout.confirmation);
                        TextView txtTitle = (TextView) dialog
                                .findViewById(R.id.txtTitle);
                        TextView txtdescription = (TextView) dialog
                                .findViewById(R.id.txtdescription);
                        txtTitle.setText("Delete Photo");
                        txtdescription
                                .setText("Are you sure you want to delete this Photo?");
                        // String array for issue title
                        // set the custom dialog components - List-view
                        // btn-cancel
                        final Button btncancel = (Button) dialog
                                .findViewById(R.id.btncancel);
                        final Button btnDone = (Button) dialog
                                .findViewById(R.id.btnDone);
                        btncancel.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                // Auto-generated method stub

                                dialog.dismiss();
                            }
                        });
                        btnDone.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                dialog.dismiss();
                                showDialog(typeBar);
                                flagService = 1;
                                WebserviceHelper helper = new WebserviceHelper(InspectionImageList.this,Constants.GLOBAL_URL);
                                RemovedPos = position;
                                helper.addParam("action", "deleteInspection");
                                helper.addParam("inspection_id", model.id);
                                helper.execute(helper.buildUrl());
                            }
                        });
                        dialog.show();
                    }
                }
            });
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(NewEditinspection.getIntent(InspectionImageList.this, model), 22222);
                    }

                }
            });
            selectitem.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    NetReachability net = new NetReachability(
                            InspectionImageList.this);
                    boolean netcheck = net.isInternetOn();
                    if (netcheck == false) {
                        Toast.makeText(
                                InspectionImageList.this,
                                "Please  check your internet connection and try again",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        startActivityForResult(NewEditinspection.getIntent(InspectionImageList.this, model), 22222);
                    }

                }

            });
            return view;
        }

    }

    public void networkPopup()
    {
        // this intent class using network checked
        Intent i = new Intent(InspectionImageList.this, NetworkPopup.class);
        startActivity(i);
    }

    @Override
    public void requestFinished(String result)
    {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray res = jsonObject.getJSONArray("W");
            String status = res.getJSONObject(0).getString("Status");
            dismissDialog(typeBar);

            switch (flagService)
            {
                case 1:
                {
                    if (status.equals("false"))
                    {
                    } else if (status.equals("true"))
                    {
                        mListMain.remove(RemovedPos);
                        myadapter.setList(mListMain);
                        if (myadapter.getCount() == 0)
                        {
                            setResult(RESULT_OK);
                            InspectionImageList.this.finish();
                            return;
                        }
                        setResult(RESULT_OK);
                    }
                }
                break;
                case 4: {

                    if (status.equals("false"))
                    {
                    } else if (status.equals("true")) {
                        mListMain.clear();
                        mListMain = ImageModel.getList(res.getJSONObject(1)
                                .getJSONArray("image_detail"));

                        myadapter.setList(mListMain);

                    }

                }
                break;
                default:
                    break;
            }

        } catch (JSONException e) {
            // TODO: handle exception
            progDialog.dismiss();
        }
    }

    int RemovedPos = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_FIRST_USER) {
            setResult(RESULT_FIRST_USER);
            this.finish();
            return;
        }

        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_ADD_IMAGE_SITE) {
            setResult(RESULT_OK);
            filter_status="0";
            callListAPI();
        }
        if (resultCode == RESULT_OK && requestCode == 12345) {
            setResult(RESULT_OK);
            callListAPI();
        }
        if (resultCode == RESULT_OK && requestCode == 22222) {
            callListAPI();
            setResult(RESULT_OK);
        }
    }


    private class CheckedTask extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute()
        {
            Constants.showProgressDialog(context);
        }

        @Override
        protected String doInBackground(String... params)
        {
            HttpCall httpCall = new HttpCall();
            String resp = httpCall.callJsnGet(params[0]);
            return resp;
        }

        protected void onPostExecute(String resp) {
            Constants.hideProgressDialog();
            callListAPI();
            setResult(RESULT_OK);
        }
    }

}
