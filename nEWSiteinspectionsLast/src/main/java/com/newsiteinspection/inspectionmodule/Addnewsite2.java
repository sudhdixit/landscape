package com.newsiteinspection.inspectionmodule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.ImageUpload;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.NetworkPopup;
import com.newsiteinspection.helper.PhotoCaptureSuport;
import com.newsiteinspection.helper.Receiver;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.utility.Adapter;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Addnewsite2 extends Activity implements RequestReceiver, Receiver {
    private Context context;
    private int typeBar = 0;
    EditText txtdescription = null, txtremarks = null;
    Button nextsecond = null;
    Button btnback = null;
    Button home = null;
    ProgressDialog progDialog;
    private int year, month, day, hour, minute, second;
    String currentDateTime = null;
    private int flagService;
    String imagepath, site_id, site_name, title, created_by, last_updated_date;
    private String imgName,remarkImgName;
    private TextView upload;
    private ImageView pic;
    public final static int SELECT_FILE = 310, SELECT_CAMERA = 311;
    private String selectedPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.addnewsite2);
        context = this;
        imagepath = getIntent().getStringExtra("imagepath");
        site_id = getIntent().getStringExtra("site_id");
        site_name = getIntent().getStringExtra("site_name");
        title = getIntent().getStringExtra("title");
        created_by = getIntent().getStringExtra("created_by");
        last_updated_date = getIntent().getStringExtra("last_updated_date");
        if (TextUtils.isEmpty(created_by))
            created_by = AppUtil.getUserId(context);
        if (TextUtils.isEmpty(last_updated_date))
            last_updated_date = getDATE();
        initView();
        setListnenr();
    }

    private void initView() {
        // TODO Auto-generated method stub
        btnback = (Button) findViewById(R.id.btnback);
        TextView username = (TextView) findViewById(R.id.username);
        home = (Button) findViewById(R.id.home);
        username.setText("Welcome " + AppUtil.getUserName(context) + " !");
        txtdescription = (EditText) findViewById(R.id.txtdescription);
        txtremarks = (EditText) findViewById(R.id.txtremarks);
        nextsecond = (Button) findViewById(R.id.nextsecond);
        upload = (TextView) findViewById(R.id.upload);
        pic = (ImageView) findViewById(R.id.pic);
    }

    @SuppressWarnings("deprecation")
    private void initData() {
        // TODO Auto-generated method stub
        String strdescription = txtdescription.getText().toString();
        String strremark = txtremarks.getText().toString();
        NetReachability net = new NetReachability(Addnewsite2.this);
        boolean netcheck = net.isInternetOn();
        if (netcheck == false) {
            Constants.ShowToast(context, "Please check your network connection");
        } else {
            showDialog(typeBar);
            flagService = 1;
            if (!TextUtils.isEmpty(imagepath))
            {
                imgName = AppUtil.getUserId(context) + "_" + getSeparatedDate() + ".jpg";
                System.out.println("globle path =" + imgName);
                ImageUpload upload = new ImageUpload(Addnewsite2.this);
                upload.execute(imagepath, imgName, "image");
            }
            if (imgName == "")
                imgName = "noimage.gif";

            if (!TextUtils.isEmpty(selectedPath))
            {
                remarkImgName = "Inpection_Status_Image_"+AppUtil.getUserId(context) + "_" + getSeparatedDate() + ".jpg";
                System.out.println("remark image path =" + remarkImgName);
                ImageUpload upload = new ImageUpload(Addnewsite2.this);
                upload.execute(selectedPath,remarkImgName, "image");
            }
            else
                remarkImgName="";

            WebserviceHelper helper = new WebserviceHelper(Addnewsite2.this, Constants.GLOBAL_URL_NEW);
            helper.addParam("action","tempaddinspection");
            helper.addParam("site",site_id);
            helper.addParam("image_name",imgName);
            helper.addParam("description", strdescription);
            helper.addParam("remark", strremark);
            helper.addParam("new_inspection_id", "");
            helper.addParam("created_by", created_by);
            helper.addParam("user_id", AppUtil.getUserId(context));
            helper.addParam("last_update_date", last_updated_date);
            helper.addParam("remark_image",remarkImgName);
            helper.execute(helper.buildUrl());
        }
    }


    private String getSeparatedDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        second = c.get(Calendar.SECOND);
        currentDateTime = DateFormate();
        // data separate string array
        String[] strdatetime = currentDateTime.split(" ");
        String date = strdatetime[0];
        String time = strdatetime[1];
        String[] strdate = date.split("-");
        String yy = strdate[0];
        String mmm = strdate[1];
        String dd = strdate[2];
        String[] strtime = time.split(":");
        String hh = strtime[0];
        String mm = strtime[1];
        String ss = strtime[2];
        String seprateDate = yy + mmm + dd + "_" + hh + mm + ss;
        return seprateDate;
    }

    public String getDATE() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        this.finish();
    }

    public String DateFormate() {

        // set selected date into date_picker also
        if (day <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append(month + 1).append("-").append("0").append(day)
                    .append(" ").append(hour).append(":").append(minute)
                    .append(":").append(second).toString();
        }
        if (month <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append("0").append(month + 1).append("-").append(day)
                    .append(" ").append(hour).append(" ").append(":")
                    .append(minute).append(":").append(second).toString();
        }
        if (day <= 9 && month <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append("0").append(month + 1).append("-").append("0")
                    .append(day).append(" ").append(hour).append(":")
                    .append(minute).append(":").append(second).toString();
        } else {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append(month + 1).append("-").append(day).append(" ")
                    .append(hour).append(":").append(minute).append(":")
                    .append(second).toString();
        }

        System.out.println("get currentDateTime =" + currentDateTime);
        return currentDateTime;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                progDialog = new ProgressDialog(this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.setCancelable(true);
                return progDialog;
            default:
                return null;
        }
    }

    private void setListnenr() {
        // TODO Auto-generated method stub
        home.setOnClickListener(laylistner);
        nextsecond.setOnClickListener(laylistner);
        btnback.setOnClickListener(laylistner);
        upload.setOnClickListener(laylistner);
    }

    OnClickListener laylistner = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.upload:
                    dialogImageOption();
                    break;
                case R.id.nextsecond:
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus()
                                .getWindowToken(), 0);
                    } catch (Exception e) {

                    }
                    initData();
                    break;
                case R.id.btnback: {
                    onBackPressed();
                }
                break;
                case R.id.home: {
                    Intent i = new Intent(context, Home.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                default:
                    break;
            }
        }
    };

    public void networkPopup() {
        // this intent class using network checked
        Intent i = new Intent(Addnewsite2.this, NetworkPopup.class);
        startActivity(i);

    }



    @Override
    public void requestFinished(String result) {
        // TODO Auto-generated method stub
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray res = jsonObject.getJSONArray("W");
            String status = res.getJSONObject(0).getString("Status");

            if (flagService == 1) {
                if (status.equals("false")) {
                    progDialog.dismiss();
                    Intent i = new Intent(this, AlertPopupScreen.class);
                    i.putExtra("utilityTitle", "Unable To Add site ...!");
                    i.putExtra("utilityMessage", "Please add site after sometime");
                    startActivity(i);
                } else if (status.equals("true")) {
                    Intent in = new Intent();
                    in.putExtra("site_id", site_id);
                    in.putExtra("last_updated_date", last_updated_date);
                    in.putExtra("user_id", created_by);
                    setResult(RESULT_OK, in);
                    this.finish();
                }
            }
        } catch (JSONException e) {
            // TODO: handle exception
            progDialog.dismiss();
        } catch (Exception e) {
            // TODO: handle exception
            progDialog.dismiss();
            networkPopup();
        }
    }

    @Override
    public void requestFinish(String result) {
        // TODO Auto-generated method stub

    }


    private void dialogImageOption()
    {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_img_picker);
        TextView take_photo=(TextView)dialog.findViewById(R.id.take_photo);
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                doTakePhotoAction();
            }
        });
        TextView choose_existing=(TextView)dialog.findViewById(R.id.choose_existing);
        choose_existing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        dialog.show();
    }

    public void openGallery()
    {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }


    private void doTakePhotoAction() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(cameraIntent, SELECT_CAMERA);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode ==RESULT_OK)
        {
            if (requestCode == SELECT_FILE)
            {
                Uri selectedImageUri = data.getData();
                selectedPath = getImagePath(selectedImageUri);
                Log.e("Himanshu", "Dixit Selected Path" + selectedPath);
                if(!TextUtils.isEmpty(selectedPath))
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize =2;
                    selectedPath=Constants.compressImage(selectedPath,context);
                    pic.setImageBitmap(BitmapFactory.decodeFile(selectedPath, options));
                    upload.setText("Change");
                }
                else
                    Constants.ShowToast(context,"No File Selected");
            }
            else if(requestCode == SELECT_CAMERA )
            {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                selectedPath = f.getAbsolutePath();
                Log.e("Himanshu","Dixit Selected Path"+selectedPath);
                if(!TextUtils.isEmpty(selectedPath))
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize =2;
                    selectedPath=Constants.compressImage(selectedPath,context);
                    pic.setImageBitmap(BitmapFactory.decodeFile(selectedPath,options));
                    upload.setText("Change");
                }
                else
                    Constants.ShowToast(context,"No File Selected");
            }
        }
    }

    public String getImagePath(Uri uri)
    {
        String result = "";
        boolean isok = false;
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(uri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            isok = true;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isok ? result : "";
    }
}
