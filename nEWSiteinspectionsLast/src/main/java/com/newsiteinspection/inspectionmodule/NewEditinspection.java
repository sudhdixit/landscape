package com.newsiteinspection.inspectionmodule;

import java.io.File;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.multiplepicker.ImageModel;
import com.multiplepicker.MediaPickerActivity;
import com.newsiteinspection.FullViewImage;
import com.newsiteinspection.Home;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.AlertPopupScreen;
import com.newsiteinspection.helper.ImageLoader;
import com.newsiteinspection.helper.ImageUpload;
import com.newsiteinspection.helper.Receiver;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;

@SuppressWarnings("unused")
public class NewEditinspection extends MediaPickerActivity implements
        OnClickListener, Receiver, RequestReceiver {
    private EditText edtdescription = null, edtremarks = null;
    private Button capture = null, browse = null, save = null, cancel, home = null;
    private Button btnback;
    private ImageView siteimage = null;
    String imageName = "";
    Context mContext;
    String description, remarks, imgname, remarkImgName;
    private ImageModel mImageModel;
    String localPath = "";
    private TextView upload;
    private ImageView pic;
    public final static int SELECT_FILE = 310, SELECT_CAMERA = 311;
    private String selectedPath;
    private int year, month, day, hour, minute, second;
    String currentDateTime = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.editinspaction);
        mContext = NewEditinspection.this;
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getIntentData();
        initViews();
        setData();

    }

    public void setData() {

        if (null != mImageModel) {
            edtdescription.setText(mImageModel.description);
            edtremarks.setText(mImageModel.remark);
            imageName = mImageModel.image_name;
            remarkImgName = mImageModel.remark_image;
            mImageLoader.DisplayImage(
                    "http://chenwa.biz/webservices/imagesuploaded/"
                            + mImageModel.image_name, siteimage);

            mImageLoader.DisplayImage(
                    "http://chenwa.biz/webservices/imagesuploaded/"
                            + mImageModel.remark_image,pic);
            if(!TextUtils.isEmpty(remarkImgName))
                upload.setText("Change");
        }
    }

    private void getIntentData() {
        mImageModel = (ImageModel) getIntent().getParcelableExtra("imgModel");

    }

    public static Intent getIntent(Context context, ImageModel imgModel) {

        Intent intent = new Intent(context, NewEditinspection.class);
        intent.putExtra("imgModel", imgModel);
        return intent;

    }

    private ImageLoader mImageLoader;

    private void initViews() {
        edtdescription = (EditText) findViewById(R.id.edtdescription);
        edtremarks = (EditText) findViewById(R.id.edtremarks);
        capture = (Button) findViewById(R.id.capture);
        browse = (Button) findViewById(R.id.browse);
        btnback = (Button) findViewById(R.id.btnback);
        home = (Button) findViewById(R.id.home);
        save = (Button) findViewById(R.id.save);
        cancel = (Button) findViewById(R.id.cancel);
        siteimage = (ImageView) findViewById(R.id.siteimage);
        upload = (TextView) findViewById(R.id.upload);
        pic = (ImageView) findViewById(R.id.pic);
        mImageLoader = new ImageLoader(this);
        capture.setOnClickListener(this);
        siteimage.setOnClickListener(this);
        browse.setOnClickListener(this);
        btnback.setOnClickListener(this);
        home.setOnClickListener(this);
        cancel.setOnClickListener(this);
        save.setOnClickListener(this);
        upload.setOnClickListener(this);
        pic.setOnClickListener(this);

    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {

            case R.id.upload:
                dialogImageOption();
                break;
            case R.id.capture:
                openGallery(222, false);
                break;
            case R.id.browse:
                openGallery(111, false);
                break;
            case R.id.btnback:
                this.finish();
                break;
            case R.id.home:
                Intent i = new Intent(NewEditinspection.this, Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
            case R.id.save: {
                CallWebServiceSave();
            }
            break;
            case R.id.cancel: {
                this.finish();
            }
            break;
            case R.id.siteimage: {
                Intent i1 = new Intent(NewEditinspection.this, FullViewImage.class);
                i1.putExtra("FRM", "SERVER");
                if (localPath.equals("")) {
                    i1.putExtra("URL",
                            "http://chenwa.biz/webservices/imagesuploaded/"
                                    + mImageModel.image_name);
                } else {
                    i1.putExtra("URL", "file://" + localPath);
                }

                startActivity(i1);
            }

            break;

            case R.id.pic: {
                Intent i1 = new Intent(NewEditinspection.this, FullViewImage.class);
                i1.putExtra("FRM", "SERVER");
                if(!TextUtils.isEmpty(selectedPath))
                {
                    i1.putExtra("URL", "file://" + localPath);
                    startActivity(i1);
                }
                else if(!TextUtils.isEmpty(remarkImgName))
                {
                    i1.putExtra("URL",
                            "http://chenwa.biz/webservices/imagesuploaded/"
                                    + mImageModel.remark_image);
                    startActivity(i1);
                }
            }

            break;

            default:
                break;
        }
    }

    private void CallWebServiceSave() {

        showDialog(0);
        if (localPath.length() > 0)
        {
            ImageUpload upload1 = new ImageUpload(this);
            upload1.execute(localPath,imageName, "image");
        }
        if (!TextUtils.isEmpty(selectedPath))
        {
            remarkImgName = "Inpection_Status_Image_"+AppUtil.getUserId(mContext) + "_" + getSeparatedDate() + ".jpg";
            System.out.println("remark image path =" + remarkImgName);
            ImageUpload upload = new ImageUpload(NewEditinspection.this);
            upload.execute(selectedPath,remarkImgName,"image");
        }

        WebserviceHelper helper = new WebserviceHelper(this, Constants.GLOBAL_URL);
        String ifd = AppUtil.getUserId(mContext);
        helper.addParam("action", "editInspection");
        helper.addParam("inspection_id", mImageModel.id);
        helper.addParam("image_name", imageName);
        helper.addParam("user_id", ifd);
        helper.addParam("description", edtdescription.getText().toString().trim());
        helper.addParam("remark", edtremarks.getText().toString().trim());
        helper.addParam("remark_image",remarkImgName);
        helper.execute(helper.buildUrl());
    }

    @Override
    protected void onSingleImageSelected(int starterCode, File fileUri,
                                         String imagPath, Bitmap bitmap) {
        if (null != bitmap) {
            siteimage.setImageBitmap(bitmap);
            localPath = imagPath;
            imageName = fileUri.getName();
        }
    }

    @Override
    protected void onVideoCaptured(int starterCode, String videoPath) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onMediaPickCanceled(int starterCode, int reqCode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void requestFinish(String result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void requestFinished(String result) {
        // TODO Auto-generated method stub
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray res = jsonObject.getJSONArray("W");
            String status = res.getJSONObject(0).getString("Status");
            if (status.equals("false")) { // result failed error show in

                mProgressDialog.dismiss();
                Intent i = new Intent(this, AlertPopupScreen.class);
                i.putExtra("utilityTitle", "Unable To Add site ...!");
                i.putExtra("utilityMessage", "Please add site after sometime");
                startActivity(i);
            } else if (status.equals("true")) {
                mProgressDialog.dismiss();
                Constants.ShowToast(mContext,"Edit Successfull");
                setResult(RESULT_OK);
                this.finish();

            }
        } catch (JSONException e) {
            // TODO: handle exception
            mProgressDialog.dismiss();
        } catch (Exception e) {
            // TODO: handle exception
            mProgressDialog.dismiss();

        }

    }

    ProgressDialog mProgressDialog;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Please wait...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setCancelable(false);
                return mProgressDialog;
            default:
                return null;
        }
    }

    private String getSeparatedDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        second = c.get(Calendar.SECOND);
        currentDateTime = DateFormate();
        // data separate string array
        String[] strdatetime = currentDateTime.split(" ");
        String date = strdatetime[0];
        String time = strdatetime[1];
        String[] strdate = date.split("-");
        String yy = strdate[0];
        String mmm = strdate[1];
        String dd = strdate[2];
        String[] strtime = time.split(":");
        String hh = strtime[0];
        String mm = strtime[1];
        String ss = strtime[2];
        String seprateDate = yy + mmm + dd + "_" + hh + mm + ss;
        return seprateDate;
    }

    public String DateFormate() {

        // set selected date into date_picker also
        if (day <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append(month + 1).append("-").append("0").append(day)
                    .append(" ").append(hour).append(":").append(minute)
                    .append(":").append(second).toString();
        }
        if (month <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append("0").append(month + 1).append("-").append(day)
                    .append(" ").append(hour).append(" ").append(":")
                    .append(minute).append(":").append(second).toString();
        }
        if (day <= 9 && month <= 9) {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append("0").append(month + 1).append("-").append("0")
                    .append(day).append(" ").append(hour).append(":")
                    .append(minute).append(":").append(second).toString();
        } else {
            currentDateTime = new StringBuilder().append(year).append("-")
                    .append(month + 1).append("-").append(day).append(" ")
                    .append(hour).append(":").append(minute).append(":")
                    .append(second).toString();
        }

        System.out.println("get currentDateTime =" + currentDateTime);
        return currentDateTime;
    }

    private void dialogImageOption()
    {
        final Dialog dialog = new Dialog(NewEditinspection.this);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_img_picker);
        TextView take_photo=(TextView)dialog.findViewById(R.id.take_photo);
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                doTakePhotoAction();
            }
        });
        TextView choose_existing=(TextView)dialog.findViewById(R.id.choose_existing);
        choose_existing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        dialog.show();
    }

    public void openGallery()
    {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }


    private void doTakePhotoAction() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(cameraIntent, SELECT_CAMERA);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode ==RESULT_OK)
        {
            if (requestCode == SELECT_FILE)
            {
                Uri selectedImageUri = data.getData();
                selectedPath = getImagePath(selectedImageUri);
                Log.e("Himanshu", "Dixit Selected Path" + selectedPath);
                if(!TextUtils.isEmpty(selectedPath))
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize =2;
                    selectedPath=Constants.compressImage(selectedPath,mContext);
                    pic.setImageBitmap(BitmapFactory.decodeFile(selectedPath, options));
                    upload.setText("Change");
                }
                else
                    Constants.ShowToast(mContext,"No File Selected");
            }
            else if(requestCode == SELECT_CAMERA )
            {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                selectedPath = f.getAbsolutePath();
                Log.e("Himanshu","Dixit Selected Path"+selectedPath);
                if(!TextUtils.isEmpty(selectedPath))
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize =2;
                    selectedPath=Constants.compressImage(selectedPath,mContext);
                    pic.setImageBitmap(BitmapFactory.decodeFile(selectedPath,options));
                    upload.setText("Change");
                }
                else
                    Constants.ShowToast(mContext,"No File Selected");
            }
        }
    }

    public String getImagePath(Uri uri)
    {
        String result = "";
        boolean isok = false;
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = mContext.getContentResolver().query(uri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            isok = true;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return isok ? result : "";
    }
}
