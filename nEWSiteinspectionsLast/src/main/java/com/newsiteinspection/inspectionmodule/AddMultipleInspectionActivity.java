package com.newsiteinspection.inspectionmodule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.multiplepicker.Action;
import com.multiplepicker.CustomGallery;
import com.multiplepicker.GalleryAdapter;
import com.newsiteinspection.R;
import com.newsiteinspection.apputility.AppUtil;
import com.newsiteinspection.apputility.Constants;
import com.newsiteinspection.helper.ImageUpload;
import com.newsiteinspection.helper.NetReachability;
import com.newsiteinspection.helper.PhotoCaptureSuport;
import com.newsiteinspection.helper.Receiver;
import com.newsiteinspection.helper.RequestReceiver;
import com.newsiteinspection.helper.WebserviceHelper;
import com.newsiteinspection.utility.Adapter;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class AddMultipleInspectionActivity extends Activity implements RequestReceiver, Receiver {

	private Context context;
	GridView gridGallery;
	Handler handler;
	GalleryAdapter adapter;
	ProgressDialog progDialog;
	Button btnGalleryPickMul;
	ViewSwitcher viewSwitcher;
	ImageLoader imageLoader;
	TextView txt_count, txt_site;
	public static String[] all_path = new String[] {};
	String imgName = "";
	int Counter = 0;
	int createdid;
	private String site_id,site_name,created_by,last_updated_date;
	private int year, month, day, hour, minute, second;
	private String currentDateTime = null;

	SharedPreferences prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		context=this;
		site_id=getIntent().getStringExtra("site_id");
		site_name=getIntent().getStringExtra("site_name");
		created_by=getIntent().getStringExtra("created_by");
		last_updated_date=getIntent().getStringExtra("last_updated_date");
		if(TextUtils.isEmpty(created_by))
			created_by= AppUtil.getUserId(context);
		if(TextUtils.isEmpty(last_updated_date))
			last_updated_date=getDATE();
		initImageLoader();
		init();
	}

	private void initImageLoader() {

		txt_count = (TextView) findViewById(R.id.txt_count);
		txt_site = (TextView) findViewById(R.id.txt_site);
		txt_count.setText("0 Image selected");
		txt_site.setText("For " + site_name);
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
				this).defaultDisplayImageOptions(defaultOptions).memoryCache(
				new WeakMemoryCache());
		ImageLoaderConfiguration config = builder.build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);

	}


	public String getDATE() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(c.getTime());
	}

	private void init() {

		handler = new Handler();
		gridGallery = (GridView) findViewById(R.id.gridGallery);
		gridGallery.setFastScrollEnabled(true);
		adapter = new GalleryAdapter(getApplicationContext(), imageLoader);
		adapter.setMultiplePick(false);
		gridGallery.setAdapter(adapter);
		viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
		viewSwitcher.setDisplayedChild(1);
		btnGalleryPickMul = (Button) findViewById(R.id.btnGalleryPickMul);
		btnGalleryPickMul.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
				startActivityForResult(i, 200);
			}
		});
	}

	public void done(View v) {

		try {
			if (all_path.length > 0)
			{
				NetReachability net = new NetReachability(AddMultipleInspectionActivity.this);
				if (net.isInternetOn())
				{
					showDialog(0);
					callweb(0);
				}
				else
				{
					Constants.ShowToast(context,"Please check your interenet connection");
				}

			}
			else
			{
				Constants.ShowToast(context, "No image selected select atleast 1 image");
			}

		} catch (NullPointerException e) {
			// TODO: handle exception
			Constants.ShowToast(context, "No image selected");
		}

	}

	public void callweb(int cnt)
	{
		imgName = AppUtil.getUserId(context) + "_" + getSeparatedDate() + ".jpg";
		ImageUpload upload1 = new ImageUpload(AddMultipleInspectionActivity.this);
		upload1.execute(AddMultipleInspectionActivity.all_path[cnt],imgName,"image");
		WebserviceHelper helper = new WebserviceHelper(this, Constants.GLOBAL_URL_NEW);
		helper.addParam("action", "tempaddinspection");
		helper.addParam("site", site_id);
		helper.addParam("image_name", imgName);
		helper.addParam("description", "");
		helper.addParam("remark", "");
		helper.addParam("new_inspection_id","");
		helper.addParam("created_by",created_by);
		helper.addParam("user_id",AppUtil.getUserId(context));
		helper.addParam("last_update_date",last_updated_date);
		helper.addParam("remark_image","");
		helper.execute(helper.buildUrl());
	}

	private String getSeparatedDate()
	{
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		second = c.get(Calendar.SECOND);
		currentDateTime = DateFormate();
		// data separate string array
		String[] strdatetime = currentDateTime.split(" ");
		String date = strdatetime[0];
		String time = strdatetime[1];
		String[] strdate = date.split("-");
		String yy = strdate[0];
		String mmm = strdate[1];
		String dd = strdate[2];
		String[] strtime = time.split(":");
		String hh = strtime[0];
		String mm = strtime[1];
		String ss = strtime[2];
		String seprateDate = yy + mmm + dd + "_" + hh + mm + ss;
		return seprateDate;
	}

	public String DateFormate() {

		// set selected date into date_picker also
		if (day <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append("0").append(day)
					.append(" ").append(hour).append(":").append(minute)
					.append(":").append(second).toString();
		}
		if (month <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append("0").append(month + 1).append("-").append(day)
					.append(" ").append(hour).append(" ").append(":")
					.append(minute).append(":").append(second).toString();
		}
		if (day <= 9 && month <= 9) {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append("0").append(month + 1).append("-").append("0")
					.append(day).append(" ").append(hour).append(":")
					.append(minute).append(":").append(second).toString();
		} else {
			currentDateTime = new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append(day).append(" ")
					.append(hour).append(":").append(minute).append(":")
					.append(second).toString();
		}

		System.out.println("get currentDateTime =" + currentDateTime);
		return currentDateTime;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			progDialog = new ProgressDialog(this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Please wait...");
			progDialog.setCanceledOnTouchOutside(false);
			progDialog.setCancelable(true);
			return progDialog;
		default:
			return null;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
			all_path = data.getStringArrayExtra("all_path");
			ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
			for (String string : all_path) {
				CustomGallery item = new CustomGallery();
				item.sdcardPath = string;
				dataT.add(item);
			}
			txt_count.setText(all_path.length + " Image Selected");
			viewSwitcher.setDisplayedChild(0);
			adapter.addAll(dataT);
		}
	}

	public void cancel(View v)
	{
		onBackPressed();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Constants.ShowToast(context, "No image selected");
		PhotoCaptureSuport.getInstance().setImage(null);
		all_path = null;
		setResult(RESULT_CANCELED);
		this.finish();
	}

	@Override
	public void requestFinish(String result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestFinished(String result) {

		try {
			JSONObject jsonObject = new JSONObject(result);
			JSONArray res = jsonObject.getJSONArray("W");
			String status = res.getJSONObject(0).getString("Status");
			PhotoCaptureSuport.getInstance().setImage(null);
			Counter++;
			if (Counter < AddMultipleInspectionActivity.all_path.length)
			{
				callweb(Counter);
			}
			else
			{
				Intent in = new Intent();
				in.putExtra("site_id",site_id);
				in.putExtra("last_updated_date",last_updated_date);
				in.putExtra("user_id",created_by);
				setResult(RESULT_OK,in);
				this.finish();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
